@tool
class_name LoadingScreen extends Control

var arc_size: float = 180
var arc_start: float = 0.0
var time: float = 0.0
@export_range(0,360)
var arc_min_length: float = 30
@export_range(0,360)
var arc_max_length: float = 270
@export
var length: float = 1.5
@export
var speed: float = 1.5
@export
var delay: float = 0.495
@export
var delay_speed: float = 90
@export
var radius: float = 20
@export
var spacing: float = 4

var tex: Texture2D
var bg: Color

# Called when the node enters the scene tree for the first time.
func _ready():
	tex = load(ProjectSettings.get_setting("application/boot_splash/image"))
	bg = ProjectSettings.get_setting("application/boot_splash/bg_color")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float):
	time = wrapf(time + delta, 0, length)
	if time < delay:
		arc_start += delta * delay_speed
		var arc_size_prev = arc_size
		arc_size = lerpf(arc_size, arc_min_length, 0.25)
		arc_start += arc_size_prev - arc_size
	else:
		var size_timer = time - delay
		size_timer /= (length - delay)
		var smaller = size_timer >= 0.5
		size_timer = size_timer * 2.0
		if smaller:
			size_timer -= 1.0
			var arc_size_prev = arc_size
			arc_size = lerpf(arc_max_length, arc_min_length, size_timer ** 2.0)
			arc_start += arc_size_prev - arc_size
			arc_start += delta * speed * arc_size
		else:
			arc_size = lerpf(arc_min_length, arc_max_length, size_timer)
			arc_start += delta * speed * arc_size
	arc_size = clamp(arc_size, arc_min_length, arc_max_length)
	arc_start = wrapf(arc_start, 0, 360)
	queue_redraw()

func _draw():
	var arc_radius = radius
	var arc_start_rad = deg_to_rad(arc_start)
	var arc_end_rad = arc_start_rad + deg_to_rad(arc_size)
	draw_rect(get_rect(), bg)
	var draw_pos = size / 2
	if tex is Texture2D:
		var tex_size = tex.get_size()
		var tex_pos = draw_pos - (tex_size / 2.0)
		draw_texture_rect(tex, Rect2(tex_pos, tex_size), false)
		draw_pos.y += tex_size.y / 2.0
	draw_pos.y += arc_radius + 2.5 + spacing
	draw_arc(draw_pos, arc_radius, arc_start_rad, arc_end_rad, arc_radius * 4.0, Color.WHITE, 4.0, false)
	draw_arc(draw_pos, arc_radius, arc_start_rad, arc_end_rad, arc_radius * 4.0, Color.WHITE * Color(1, 1, 1, 0.5), 5.0, false)
