@tool
extends LoadingScreen

var dragging: bool = false
var mousePos: Vector2i = Vector2i.ZERO

func _process(delta):
	super(delta)
	if dragging:
		var newMousePos = DisplayServer.mouse_get_position()
		var titlebar_global_end_y = get_viewport().position.y + get_rect().size.y
		var titlebar_size = get_rect().size
		var winPos = get_viewport().position
		var winMousePos = newMousePos - winPos
		var winSize = get_viewport().size
		if get_viewport().mode == Window.MODE_MAXIMIZED:
			if newMousePos.y > titlebar_global_end_y:
				get_viewport().mode = Window.MODE_WINDOWED
				get_viewport().position = newMousePos - Vector2i(get_viewport().size.x / 4.0, winMousePos.y)
		else:
			var usable_rect: Rect2i = DisplayServer.screen_get_usable_rect((get_viewport() as Window).current_screen)
			if mousePos.y <= usable_rect.position.y:
				get_viewport().mode = Window.MODE_MAXIMIZED
			else:
				get_viewport().position += newMousePos - mousePos
		mousePos = newMousePos
		dragging = DisplayServer.mouse_get_button_state() & MOUSE_BUTTON_MASK_LEFT

func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.double_click:
				get_viewport().mode = Window.MODE_MAXIMIZED if get_viewport().mode == Window.MODE_WINDOWED else Window.MODE_WINDOWED
			else:
				mousePos = DisplayServer.mouse_get_position()
				dragging = event.pressed
