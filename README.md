# Moved
Use [this repo](https://complecwaft.com/CatOrganization/looping-audio-player)
# Looping Audio Player
This is an audio player that loops audio in such a way that it can specify how to loop.

Currently only works correctly with .wav files and .ogg files with or without seamless loop metadata, and .mp3 files without seamless loop metadata.
