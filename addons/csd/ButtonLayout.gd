class_name WinBtnLayout extends "res://config/Serializable.gd"

enum Entry {
	Spacer,
	Close,
	Minimize,
	Maximize
}

@export
var left_buttons: Array[Entry] = []
@export
var right_buttons: Array[Entry] = []
