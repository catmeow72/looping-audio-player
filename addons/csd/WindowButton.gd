@tool
class_name WindowButton extends IconButton

enum Type {
	CLOSE,
	MIN,
	MAX,
}

@export
var type: Type = Type.CLOSE

func _get_minimum_size():
	return Vector2.ONE * 32

func _ready():
	_process(0)
	queue_redraw()
	

var mode_prev = null
func _process(delta):
	match type:
		Type.CLOSE:
			icon = CSDTheming.close_icon
		Type.MAX:
			if mode_prev != get_window().mode:
				icon = CSDTheming.max_icon if get_window().mode == Window.MODE_WINDOWED else CSDTheming.restore_icon
				queue_redraw()
				mode_prev = get_window().mode
		Type.MIN:
			icon = CSDTheming.min_icon

func _pressed():
	if Engine.is_editor_hint():
		return
	var win = get_window()
	match type:
		Type.CLOSE:
			if win == get_tree().root:
				get_tree().quit()
			else:
				win.hide()
		Type.MIN:
			win.mode = Window.MODE_MINIMIZED
		Type.MAX:
			win.mode = Window.MODE_MAXIMIZED if win.mode == Window.MODE_WINDOWED else Window.MODE_WINDOWED
