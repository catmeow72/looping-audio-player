extends Control

@onready var MaximizedView: Panel = $MaximizedView
@onready var Shadow: Panel = $Shadow
@onready var vbox_container: VBoxContainer = $VBoxContainer
var csd: bool = true

func _get_minimum_size():
	if vbox_container == null:
		return Vector2.ZERO
	return vbox_container.get_combined_minimum_size()

func _enter_tree():
	Configuration.applying.connect(func(cfg: Config):
		var win: Window = get_viewport() as Window
		csd = cfg.enable_csd
		if csd:
			win.transparent_bg = true
			win.transparent = true
			win.borderless = true
		else:
			win.transparent_bg = false
			win.transparent = false
			win.borderless = false
	)

func _ready():
	update_minimum_size()
	vbox_container.minimum_size_changed.connect(func():
		update_minimum_size()
	)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var win = get_window()
	var windowed = csd && win.mode == Window.MODE_WINDOWED
	MaximizedView.visible = !windowed
	clip_children = CanvasItem.CLIP_CHILDREN_AND_DRAW if windowed else CanvasItem.CLIP_CHILDREN_DISABLED
	if windowed:
		offset_left = 8
		offset_top = 8
		offset_right = -8
		offset_bottom = -8
	else:
		offset_left = 0
		offset_top = 0
		offset_right = 0
		offset_bottom = 0
	Shadow.offset_left = offset_left
	Shadow.offset_top = offset_top
	Shadow.offset_right = offset_right
	Shadow.offset_bottom = offset_bottom
