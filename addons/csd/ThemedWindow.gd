class_name ThemedWindow extends Window

var window_base: PackedScene = preload("res://addons/csd/WindowBase.tscn")

func update_theme(new_theme: Theme):
	theme = new_theme

# Called when the node enters the scene tree for the first time.
func _ready():
	if !Engine.is_editor_hint():
		Configuration.theme_update.connect(update_theme)
		theme.set_constant("resize_margin", "Window", 8)

func _process(delta):
	pass
