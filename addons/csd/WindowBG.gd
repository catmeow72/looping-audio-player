extends Control

@onready var panel: Panel = $Panel
@onready var shadow: Panel = $Panel/Shadow

func _get_minimum_size():
	if panel == null:
		return Vector2.ZERO
	return panel.get_combined_minimum_size() + Vector2(16, 16)

# Called when the node enters the scene tree for the first time.
func _ready():
	Configuration.theme_update.connect(func(theme):
		get_tree().root.theme = theme
	)
	show()
	var vp = get_viewport()
	update_minimum_size()
	panel.minimum_size_changed.connect(func():
		update_minimum_size()
	)

var lower_x: bool = false
var higher_x: bool = false
var lower_y: bool = false
var higher_y: bool = false
var anchor_y: int
var anchor_x: int
var starting_mouse_pos: Vector2i
var starting_size: Vector2
var mouse_motion: Vector2

func _process(delta):
	var global_mouse_pos: Vector2i = DisplayServer.mouse_get_position()
	var rect = get_viewport_rect()
	rect.position += Vector2(8, 8)
	rect.end -= Vector2(16, 16)
	top_level = dragging || !rect.has_point(global_mouse_pos)
	var win = get_window()
	win.min_size = get_combined_minimum_size()
	if dragging:
		win.size = Vector2i((global_mouse_pos - starting_mouse_pos) * Vector2i(-1 if lower_x else 1 if higher_x else 0, -1 if lower_y else 1 if higher_y else 0)) + Vector2i(starting_size)
		var usable = DisplayServer.screen_get_usable_rect()
		var min_pos = usable.position
		var max_pos = usable.end
		if lower_x:
			win.position.x = anchor_x - win.size.x
			win.size.x = min(win.size.x, anchor_x - min_pos.x)
		if lower_y:
			win.position.y = anchor_y - win.size.y
			win.size.y = min(win.size.y, anchor_y - min_pos.y)
		if higher_x:
			win.size.x = max(win.size.x, anchor_x - max_pos.x)
			win.position.x = anchor_x
		if higher_y:
			win.size.y = max(win.size.y, anchor_y - max_pos.y)
			win.position.y = anchor_y
	else:
		win.size = Vector2(max(win.min_size.x, win.size.x), max(win.min_size.y, win.size.y))

var dragging: bool = false

func _gui_input(event):
	if event is InputEventMouse:
		var size = get_viewport_rect().size
		var pos = event.position
		var pos_x_lower = pos.x <= 8
		var pos_x_higher = pos.x >= size.x - 8
		var pos_y_lower = pos.y <= 8
		var pos_y_higher = pos.y >= size.y - 8
		var horizontal = pos_x_lower || pos_x_higher
		var vertical = pos_y_lower || pos_y_higher
		var diagonal = horizontal && vertical
		if !dragging:
			if diagonal:
				mouse_default_cursor_shape = CURSOR_BDIAGSIZE if pos_x_lower != pos_y_lower || pos_x_higher != pos_y_higher else CURSOR_FDIAGSIZE
			else:
				mouse_default_cursor_shape = CURSOR_VSIZE if vertical else CURSOR_HSIZE
		if event is InputEventMouseButton:
			accept_event()
			var mb_event = event as InputEventMouseButton
			var win = get_window()
			if mb_event.is_pressed():
				if !dragging:
					starting_mouse_pos = DisplayServer.mouse_get_position()
					starting_size = win.size
					dragging = true
				else:
					return
			else:
				dragging = false
				return
			lower_x = pos_x_lower
			higher_x = pos_x_higher
			lower_y = pos_y_lower
			higher_y = pos_y_higher
			anchor_x = win.position.x
			anchor_y = win.position.y
			if lower_x:
				anchor_x += win.size.x
			if lower_y:
				anchor_y += win.size.y
