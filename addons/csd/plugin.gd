@tool
extends EditorPlugin

const THEME_SINGLETON_NAME = "CSDTheming"
const THEME_SINGLETON_PATH = "res://addons/csd/theming.gd"
var child
func _enter_tree():
	child = preload(THEME_SINGLETON_PATH).new()
	add_child(child)
	child.init_themes()
	add_autoload_singleton(THEME_SINGLETON_NAME, THEME_SINGLETON_PATH)

func _exit_tree():
	remove_child(child)
	remove_autoload_singleton(THEME_SINGLETON_NAME)
