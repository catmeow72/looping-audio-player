extends PanelContainer

@onready
var titlebar_container = $MarginContainer/TitlebarContainer
var csd: bool = true

func _ready():
	Configuration.applying.connect(apply)
	Configuration.window_btn_update.connect(apply_layout)
	apply(Configuration.config)
	apply_layout(Configuration.layout)

func apply(cfg: Config):
	var win: Window = get_window()
	csd = cfg.enable_csd
	if csd:
		get_tree().call_group("CSD", "show")
		show()
	else:
		get_tree().call_group("CSD", "hide")
		for i in titlebar_container.get_children():
			if i is Control:
				if i.visible && i.name != "RightBtns" && i.name != "LeftBtns":
					return
		hide()

func apply_layout(layout: WinBtnLayout):
	for i in [[$MarginContainer/TitlebarContainer/LeftBtns, layout.left_buttons], [$MarginContainer/TitlebarContainer/RightBtns, layout.right_buttons]]:
		var container: BoxContainer = i[0]
		var entries: Array[WinBtnLayout.Entry] = i[1]
		for child in container.get_children(true):
			container.remove_child(child)
			child.queue_free()
		for entry in entries:
			var btn: Control = null
			if entry == WinBtnLayout.Entry.Spacer:
				btn = Control.new()
				btn.custom_minimum_size = Vector2i(4, 0)
			else:
				btn = WindowButton.new()
				match entry:
					WinBtnLayout.Entry.Close:
						btn.type = WindowButton.Type.CLOSE
					WinBtnLayout.Entry.Maximize:
						btn.type = WindowButton.Type.MAX
					WinBtnLayout.Entry.Minimize:
						btn.type = WindowButton.Type.MIN
			if is_instance_valid(btn):
				container.add_child(btn)

var dragging: bool = false
var mousePos: Vector2i = Vector2i.ZERO
var winPos: Vector2i = Vector2i.ZERO

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if dragging && csd:
		var newMousePos = DisplayServer.mouse_get_position()
		var titlebar_global_end_y = get_window().position.y + get_rect().size.y
		var titlebar_size = get_rect().size
		var winMousePos = newMousePos - winPos
		var winSize = get_window().size
		if get_window().mode == Window.MODE_MAXIMIZED:
			if newMousePos.y > titlebar_global_end_y:
				get_window().mode = Window.MODE_WINDOWED
				get_window().position = newMousePos - Vector2i(get_window().size.x / 4.0, winMousePos.y)
		else:
			var usable_rect: Rect2i = DisplayServer.screen_get_usable_rect(get_window().current_screen)
			if mousePos.y <= usable_rect.position.y:
				get_window().mode = Window.MODE_MAXIMIZED
			else:
				get_window().position = newMousePos - (mousePos - winPos)
		dragging = DisplayServer.mouse_get_button_state() & MOUSE_BUTTON_MASK_LEFT

var mouse_pressed_prev: bool = false

func _gui_input(event):
	if !csd:
		return
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.double_click:
				get_window().mode = Window.MODE_MAXIMIZED if get_window().mode == Window.MODE_WINDOWED else Window.MODE_WINDOWED
			else:
				if mouse_pressed_prev != event.pressed:
					mousePos = DisplayServer.mouse_get_position()
					winPos = get_window().position
					mouse_pressed_prev = event.pressed
				dragging = event.pressed
