@tool
extends Node

var close_icon: SVGTexture
var max_icon: SVGTexture
var restore_icon: SVGTexture
var min_icon: SVGTexture

var dark_mode: bool:
	get:
		return Engine.is_editor_hint() || Configuration.is_dark_mode()

func update_icons():
	var classes = PackedStringArray(["dark" if dark_mode else "light"])
	close_icon.classes = classes
	max_icon.classes = classes
	restore_icon.classes = classes
	min_icon.classes = classes

# Called when the node enters the scene tree for the first time.
func init_themes():
	var default_theme = ThemeDB.get_default_theme()
	
	close_icon = load("res://addons/csd/icons/close.svg")
	max_icon = load("res://addons/csd/icons/maximize.svg")
	restore_icon = load("res://addons/csd/icons/restore.svg")
	min_icon = load("res://addons/csd/icons/minimize.svg")
	update_icons()
	if !Engine.is_editor_hint():
		Configuration.theme_update.connect(func(_theme):
			update_icons()
		)

func _enter_tree():
	init_themes()
