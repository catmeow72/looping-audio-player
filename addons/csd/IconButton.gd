@tool
class_name IconButton extends BaseButton

@export
var icon: Texture2D = null:
	get:
		return icon
	set(value):
		if icon != value:
			icon = value
			queue_redraw()

func _get_minimum_size():
	return Vector2.ONE * 32

func _has_point(position):
	var min_size = get_minimum_size()
	var center = min_size / 2.0
	var rect = Rect2((get_rect().size / 2.0) - center, min_size)
	return rect.has_point(position)

func _draw():
	var state = get_draw_mode()
	var stylebox = get_theme_stylebox("normal", "IconButton")
	var icon_modulate = get_theme_color("mono_color", "Editor")
	var pressed = !disabled && (state == DRAW_PRESSED || state == DRAW_HOVER_PRESSED)
	if !disabled:
		var hovering = state == DRAW_HOVER
		if hovering:
			stylebox = get_theme_stylebox("hover", "IconButton")
		if pressed:
			stylebox = get_theme_stylebox("pressed", "IconButton")
	else:
		stylebox = get_theme_stylebox("disabled", "IconButton")
	var min_size = get_minimum_size()
	var center = min_size / 2.0
	var rect = Rect2((get_rect().size / 2.0) - center, min_size)
	draw_style_box(stylebox, rect);
	if icon != null:
		var icon_rect = rect.grow(-6)
		draw_texture_rect(icon, icon_rect, false, icon_modulate)
