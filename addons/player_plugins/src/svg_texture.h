#ifndef SVG_TEXTURE_H
#define SVG_TEXTURE_H

#include "godot_cpp/classes/ref_counted.hpp"
#include "godot_cpp/templates/hashfuncs.hpp"
#include "godot_cpp/variant/dictionary.hpp"
#include "godot_cpp/variant/packed_byte_array.hpp"
#include "godot_cpp/variant/vector2i.hpp"
#include <cstdint>
#include <godot_cpp/variant/typed_array.hpp>
#include <godot_cpp/core/object.hpp>
#include <godot_cpp/classes/texture2d.hpp>
#include <godot_cpp/classes/image.hpp>
#include <godot_cpp/classes/resource.hpp>
#include <godot_cpp/classes/file_access.hpp>
#include <godot_cpp/templates/hash_map.hpp>
#include <godot_cpp/templates/hash_set.hpp>
#include <librsvg/rsvg.h>
#include <tinyxml2.h>

using namespace godot;
using namespace tinyxml2;

class SVGTexture : public Texture2D {
	GDCLASS(SVGTexture, Texture2D);
protected:
	static void _bind_methods();
	float scale;
	RsvgHandle *handle;
	XMLDocument *base_document;
	double width;
	double height;
	bool loaded;
	PackedStringArray classes;
	PackedByteArray derived_document;
	mutable HashMap<int64_t, RID> textures;

public:
	void set_scale(float scale);
	float get_scale() const;
	PackedStringArray get_classes() const;
	void set_classes(PackedStringArray classes);
	void redraw();
	void load_from_utf8_buffer(PackedByteArray content);
	void load_from_str(String contents);
	void load_from_file(Ref<FileAccess> file_access);
	static Ref<SVGTexture> load(String path);
	int32_t _get_width() const override;
	int32_t _get_height() const override;
	bool _has_alpha() const override;
	Ref<Image> _gen_image(const Vector2 size, const Rect2 src_rect = Rect2()) const;
	RID _gen_tex(const Vector2 size, const Rect2 src_rect = Rect2()) const;
	void _draw(const RID &to_canvas_item, const Vector2 &pos, const Color &modulate, bool transpose) const override;
	void _draw_rect(const RID &to_canvas_item, const Rect2 &rect, bool tile, const Color &modulate, bool transpose) const override;
	void _draw_rect_region(const RID &to_canvas_item, const Rect2 &rect, const Rect2 &src_rect, const Color &modulate, bool transpose, bool clip_uv) const override;

	SVGTexture();
	~SVGTexture();
};

#endif // SVG_TEXTURE_H
