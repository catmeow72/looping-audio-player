#include "svg_loader.h"
#include "godot_cpp/variant/packed_string_array.hpp"
#include "godot_cpp/classes/file_access.hpp"
#include <godot_cpp/classes/json.hpp>
#include "godot_cpp/variant/typed_array.hpp"
#include <godot_cpp/templates/hash_map.hpp>
#include <godot_cpp/variant/packed_string_array.hpp>
#include "svg_texture.h"

void SVGLoader::_bind_methods() {

}

Variant SVGLoader::_load(const String &path, const String &original_path, bool use_sub_threads, int32_t cache_mode) const {
    Ref<SVGTexture> texture;
    if (path.ends_with(".svgtex")) {
        texture.instantiate();
        Ref<FileAccess> json_file = FileAccess::open(path, FileAccess::READ);
        Variant parsed = JSON::parse_string(json_file->get_pascal_string());
        if (parsed.has_key("scale")) {
            Variant value = parsed.get("scale");
            if (Variant::can_convert(value.get_type(), Variant::FLOAT)) {
                texture->set_scale((float)parsed.get("scale"));
            }
        }
        if (parsed.has_key("classes")) {
            Variant value = parsed.get("classes");
            if (Variant::can_convert(value.get_type(), Variant::PACKED_STRING_ARRAY)) {
                texture->set_classes((PackedStringArray)parsed.get("classes"));
            }
        }
        texture->load_from_str(json_file->get_pascal_string());
    } else {
        SVGTexture::load(original_path);
    }

    return texture;
}


PackedStringArray SVGLoader::_get_recognized_extensions() const {
    PackedStringArray output;
    output.append("svg");
    output.append("svgtex");
    return output;
}

bool SVGLoader::_handles_type(const StringName &type) const {
    return type == StringName("SVGTexture");
}

String SVGLoader::_get_resource_type(const String &path) const {
    return (path.ends_with(".svg") || path.ends_with(".svgtex")) ? "SVGTexture" : "";
}