/*************************************************************************/
/*  editor_themes.h                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           GODOT ENGINE                                */
/*                      https://godotengine.org                          */
/*************************************************************************/
/* Copyright (c) 2007-2022 Juan Linietsky, Ariel Manzur.                 */
/* Copyright (c) 2014-2022 Godot Engine contributors (cf. AUTHORS.md).   */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#ifndef EDITOR_THEMES_H
#define EDITOR_THEMES_H

#include <godot_cpp/classes/texture.hpp>
#include <godot_cpp/classes/theme.hpp>
#include <godot_cpp/variant/color.hpp>
#include <godot_cpp/variant/string_name.hpp>
#include <godot_cpp/templates/hash_map.hpp>
#include <godot_cpp/templates/hash_set.hpp>
#include <godot_cpp/core/object.hpp>
#include <godot_cpp/classes/style_box.hpp>
#include <godot_cpp/classes/style_box_flat.hpp>
#include <godot_cpp/classes/style_box_line.hpp>
#include <godot_cpp/classes/style_box_texture.hpp>
#include <godot_cpp/classes/style_box_empty.hpp>
#include <godot_cpp/classes/thread.hpp>
#include <godot_cpp/classes/worker_thread_pool.hpp>
#include <godot_cpp/variant/callable.hpp>

#define SNAME(name) name
using namespace godot;

class CustomTheme : public Theme {
	GDCLASS(CustomTheme, Theme);
protected:
	static void _bind_methods();
	Color accent_color;
	Color base_color;
	float contrast;
	int border_size;
	int progress_bar_border_size;
	int corner_radius;
	float relationship_line_opacity;
	float spacing;
	Ref<Theme> base;
	Ref<Theme> overrides;
	float scale = 1;
	
	Ref<StyleBoxTexture> make_stylebox(Ref<Texture2D> p_texture, float p_left, float p_top, float p_right, float p_bottom, float p_margin_left = -1, float p_margin_top = -1, float p_margin_right = -1, float p_margin_bottom = -1, bool p_draw_center = true);
	Ref<StyleBoxFlat> make_circle(Ref<StyleBoxFlat> p_style);
	Ref<StyleBoxEmpty> make_empty_stylebox(float p_margin_left = -1, float p_margin_top = -1, float p_margin_right = -1, float p_margin_bottom = -1);
	Ref<StyleBoxFlat> make_flat_stylebox(Color p_color, float p_margin_left = -1, float p_margin_top = -1, float p_margin_right = -1, float p_margin_bottom = -1, int p_corner_width = 0);
	Ref<StyleBoxLine> make_line_stylebox(Color p_color, int p_thickness = 1, float p_grow_begin = 1, float p_grow_end = 1, bool p_vertical = false);
public: 
	void set_scale(float scale);
	float get_scale();
	void set_accent_color(Color accent);
	Color get_accent_color() const;
	void set_base_color(Color base);
	Color get_base_color() const;
	void set_contrast(float contrast);
	float get_contrast() const;
	void set_border_size(int border_size);
	int get_border_size() const;
	void set_progress_bar_border_size(int border_size);
	int get_progress_bar_border_size() const;
	void set_spacing(float spacing);
	float get_spacing() const;
	void set_corner_radius(int corner_radius);
	int get_corner_radius() const;
	void set_relationship_line_opacity(float opacity);
	float get_relationship_line_opacity() const;
	bool is_dark_theme() const;
	void convert_dark_theme(bool dark);
	Ref<Theme> get_base() const;
	void set_base(Ref<Theme> theme);
	Ref<Theme> get_overrides() const;
	void set_overrides(Ref<Theme> theme);
	void update_icon(StringName name, StringName type, StringName from_name, StringName from_type);
	void update();
	CustomTheme();
};

#endif // EDITOR_THEMES_H
