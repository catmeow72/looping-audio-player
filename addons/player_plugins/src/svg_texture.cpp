#include "svg_texture.h"
#include "cairo.h"
#include "godot_cpp/classes/file_access.hpp"
#include "godot_cpp/core/error_macros.hpp"
#include "godot_cpp/core/object.hpp"
#include "godot_cpp/variant/packed_byte_array.hpp"
#include "godot_cpp/variant/packed_float64_array.hpp"
#include "godot_cpp/variant/packed_int64_array.hpp"
#include "godot_cpp/variant/packed_string_array.hpp"
#include "godot_cpp/variant/typed_array.hpp"
#include "godot_cpp/variant/utility_functions.hpp"
#include "godot_cpp/variant/vector2i.hpp"
#include "tinyxml2.h"

#include <godot_cpp/core/memory.hpp>
#include <godot_cpp/templates/vector.hpp>
#include <godot_cpp/templates/hash_map.hpp>
#include <godot_cpp/classes/rendering_server.hpp>
void SVGTexture::_bind_methods() {
    ClassDB::bind_method(D_METHOD("set_scale", "scale"), &SVGTexture::set_scale);
    ClassDB::bind_method(D_METHOD("get_scale"), &SVGTexture::get_scale);
    ClassDB::bind_method(D_METHOD("set_classes", "classes"), &SVGTexture::set_classes);
    ClassDB::bind_method(D_METHOD("get_classes"), &SVGTexture::get_classes);
    ClassDB::bind_method(D_METHOD("redraw"), &SVGTexture::redraw);
    ClassDB::bind_method(D_METHOD("load_from_utf8_buffer", "buffer"), &SVGTexture::load_from_utf8_buffer);
    ClassDB::bind_method(D_METHOD("load_from_str", "contents"), &SVGTexture::load_from_str);
    ClassDB::bind_method(D_METHOD("load_from_file", "file_access"), &SVGTexture::load_from_file);
    ClassDB::bind_static_method("SVGTexture", D_METHOD("load", "path"), &SVGTexture::load);

    ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "scale"), "set_scale", "get_scale");
    ADD_PROPERTY(PropertyInfo(Variant::PACKED_STRING_ARRAY, "classes"), "set_classes", "get_classes");
}

void walk_tree(XMLElement *node, PackedStringArray classes) {
    if (node == nullptr) {
        return;
    }
    XMLNode *child = node->FirstChild();
    while (child != nullptr) {
        XMLElement *el = child->ToElement();
        if (el == nullptr) {
            child = child->NextSibling();
            continue;
        }
        walk_tree(el, classes);
        String attribute = el->Attribute("class");
        for (int i = 0; i < classes.size(); i++) {
            attribute = attribute +  String(" ") + (String)classes[i];
        }
        el->SetAttribute("class", attribute.ascii().get_data());
        child = child->NextSibling();
    }
}

void SVGTexture::redraw() {
    if (base_document == nullptr) {
        return;
    }
    derived_document = PackedByteArray();
    XMLDocument *new_document = memnew(XMLDocument);
    base_document->DeepCopy(new_document);
    XMLElement *root = new_document->RootElement();
    XMLPrinter *printer = memnew(XMLPrinter);
    walk_tree(root, classes);
    new_document->Print(printer);

    derived_document.resize(printer->CStrSize());
    for (int i = 0; i < derived_document.size(); i++) {
        derived_document.ptrw()[i] = printer->CStr()[i];
    }
    memfree(printer);
    memfree(new_document);
    GError *error = nullptr;
    if (handle != nullptr) {
        g_object_unref(G_OBJECT(handle));
    }
    handle = rsvg_handle_new_from_data(derived_document.ptr(), derived_document.size(), &error);
    ERR_FAIL_NULL_MSG(handle, error->message);
    if (error != nullptr) {
        memfree(error);
    }
    rsvg_handle_set_dpi(handle, 96);
    ERR_FAIL_COND_MSG(!rsvg_handle_get_intrinsic_size_in_pixels(handle, &width, &height), "Could not obtain image size!");
    loaded = true;
    UtilityFunctions::print_verbose("Reloaded '", get_path(), "'!");
}

void SVGTexture::set_classes(PackedStringArray classes) {
    this->classes = classes;
    redraw();
}

PackedStringArray SVGTexture::get_classes() const {
    return classes;
}

void SVGTexture::load_from_utf8_buffer(PackedByteArray p_buffer) {
    if (base_document != nullptr) {
        memfree(base_document);
        base_document = nullptr;
    }
    base_document = memnew(XMLDocument);
    base_document->Parse((const char*)p_buffer.ptr(), p_buffer.size());
    redraw();
}

void SVGTexture::load_from_str(String contents) {
    load_from_utf8_buffer(contents.to_utf8_buffer());
}

void SVGTexture::load_from_file(Ref<FileAccess> file_access) {
    ERR_FAIL_NULL(file_access);
    set_path(file_access->get_path());
    String contents = file_access->get_as_text();
    load_from_str(contents);
}

Ref<SVGTexture> SVGTexture::load(String path) {
    String file = FileAccess::get_file_as_string(path);
    Ref<SVGTexture> output = memnew(SVGTexture);
    output->load_from_str(file);
    output->set_path(path);
    return output;
}


int32_t SVGTexture::_get_width() const {
    return width * scale;
}
int32_t SVGTexture::_get_height() const {
    return height * scale;
}
bool SVGTexture::_has_alpha() const {
    return true;
}

Ref<Image> SVGTexture::_gen_image(const Vector2 size, const Rect2 src_rect) const {
    if (!loaded) {
        return nullptr;
    }
    uint32_t width = floor(size.x);
    uint32_t height = floor(size.y);
    Vector2 src_to_dst = size / get_size();
    Rect2 vp_rect;
    if (src_rect.has_area()) {
        vp_rect = Rect2(-(src_rect.position) * src_to_dst, ((src_rect.get_size()) * src_to_dst));
        vp_rect.size += vp_rect.position * 2.0;
    } else {
        vp_rect = Rect2(0, 0, size.x, size.y);
    }
    RsvgRectangle viewport = {
        .x = vp_rect.position.x,
        .y = vp_rect.position.y,
        .width = vp_rect.size.x,
        .height = vp_rect.size.y,
    };
    cairo_surface_t *surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
	cairo_t *cr = cairo_create(surface);
    GError *error = NULL;
    ERR_FAIL_COND_V_MSG(!rsvg_handle_render_document(handle, cr, &viewport, &error), nullptr, String("Could not render SVG: ") + String(error->message));

    cairo_surface_flush(surface);

	uint32_t *buffer = (uint32_t*)cairo_image_surface_get_data(surface);
    ERR_FAIL_NULL_V(buffer, nullptr);

	PackedByteArray image;
	image.resize(width * height * sizeof(uint32_t));

	for (uint32_t y = 0; y < height; y++) {
		for (uint32_t x = 0; x < width; x++) {
			uint32_t n = buffer[y * width + x];
			const size_t offset = sizeof(uint32_t) * width * y + sizeof(uint32_t) * x;
			image[offset + 0] = ((n >> 16) & 0xff);
			image[offset + 1] = ((n >> 8) & 0xff);
			image[offset + 2] = (n & 0xff);
			image[offset + 3] = ((n >> 24) & 0xff);
		}
	}
    Ref<Image> output = memnew(Image);
	output->set_data(width, height, false, Image::FORMAT_RGBA8, image);
    image.clear();
    cairo_surface_finish(surface);
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
    return output;
}
RID SVGTexture::_gen_tex(const Vector2 size, const Rect2 src_rect) const {
    PackedByteArray arr = PackedByteArray(derived_document);
    PackedFloat64Array tmp = PackedFloat64Array();
    tmp.append(size.x);
    tmp.append(size.y);
    tmp.append(src_rect.position.x);
    tmp.append(src_rect.position.y);
    tmp.append(src_rect.size.x);
    tmp.append(src_rect.size.y);
    arr.append_array(tmp.to_byte_array());
    tmp.clear();
    int64_t key = UtilityFunctions::hash(arr);
    arr.clear();

    if (!textures.has(key)) {
        Vector2 src_start = src_rect.position;
        Vector2 src_end = src_rect.get_end();
        String path = get_path();
        if (path.is_empty()) {
            path = "<unspecified>";
        } else {
            path = path.get_file();
        }
        UtilityFunctions::print_verbose("Rendering ", path, " from ", src_start.x, ",", src_start.y, " to ", src_end.x, ",", src_end.y, ", at size ", size.x, "x", size.y, "... (ID: ", key, ")");
        Ref<Image> img = _gen_image(size, src_rect);
        RenderingServer* rs = RenderingServer::get_singleton();
        textures.insert(key, rs->texture_2d_create(img));
        while (img->get_reference_count() > 0) {
            img->unreference();
        }
    }
    return textures[key];
}

void SVGTexture::_draw(const RID &to_canvas_item, const Vector2 &pos, const Color &modulate, bool transpose) const {
    RenderingServer *rs = RenderingServer::get_singleton();
    rs->canvas_item_add_texture_rect(to_canvas_item, Rect2(pos, get_size()), _gen_tex(get_size()), false, modulate, transpose);
}

void SVGTexture::_draw_rect(const RID &to_canvas_item, const Rect2 &rect, bool tile, const Color &modulate, bool transpose) const {
    RenderingServer *rs = RenderingServer::get_singleton();
    rs->canvas_item_add_texture_rect(to_canvas_item, rect, _gen_tex(rect.size), tile, modulate, transpose);
}

void SVGTexture::_draw_rect_region(const RID &to_canvas_item, const Rect2 &rect, const Rect2 &src_rect, const Color &modulate, bool transpose, bool clip_uv) const {
    RenderingServer *rs = RenderingServer::get_singleton();
    rs->canvas_item_add_texture_rect(to_canvas_item, rect, _gen_tex(rect.size, src_rect), false, modulate, transpose);
}

float SVGTexture::get_scale() const {
    return scale;
}

void SVGTexture::set_scale(float scale) {
    this->scale = MAX(scale, 0.001);
    redraw();
}

SVGTexture::SVGTexture() {
    base_document = nullptr;
    handle = nullptr;
    derived_document = PackedByteArray();
    set_scale(1.0);
    width = 0.0;
    height = 0.0;
    loaded = false;
    classes = {};
}

SVGTexture::~SVGTexture() {
    if (base_document != nullptr) {
        memfree(base_document);
    }
    RenderingServer *rs = RenderingServer::get_singleton();
    for (auto i : textures) {
        rs->free_rid(i.value);
    }
    textures.clear();
    if (handle != nullptr) {
        g_object_unref(G_OBJECT(handle));
    }
}
