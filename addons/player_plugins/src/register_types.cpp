#include "register_types.h"

#include <gdextension_interface.h>

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/defs.hpp>
#include <godot_cpp/godot.hpp>
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/classes/resource_loader.hpp>

#include "editor_themes.h"
#include "ogg_loader.h"
#include "svg_texture.h"
#include "svg_loader.h"

using namespace godot;

static Ref<SVGLoader> svg_loader;
static Ref<OggLoader> ogg_loader;

void initialize_themes_module(ModuleInitializationLevel p_level) {
	if (p_level != MODULE_INITIALIZATION_LEVEL_SCENE) {
		return;
	}

	ClassDB::register_class<CustomTheme>();
	ClassDB::register_class<SVGTexture>();
	ClassDB::register_abstract_class<SVGLoader>();
	ClassDB::register_abstract_class<OggLoader>();
	svg_loader.instantiate();
	ResourceLoader::get_singleton()->add_resource_format_loader(svg_loader);
	ogg_loader.instantiate();
	Engine::get_singleton()->register_singleton("OggLoader", ogg_loader.ptr());
}

void uninitialize_themes_module(ModuleInitializationLevel p_level) {
	if (p_level != MODULE_INITIALIZATION_LEVEL_SCENE) {
		return;
	}
	ogg_loader.unref();
	ResourceLoader::get_singleton()->remove_resource_format_loader(svg_loader);
	svg_loader.unref();
}

extern "C" {
// Initialization.
GDExtensionBool GDE_EXPORT player_plugins_library_init(const GDExtensionInterface *p_interface, GDExtensionClassLibraryPtr p_library, GDExtensionInitialization *r_initialization) {
	godot::GDExtensionBinding::InitObject init_obj(p_interface, p_library, r_initialization);

	init_obj.register_initializer(initialize_themes_module);
	init_obj.register_terminator(uninitialize_themes_module);
	init_obj.set_minimum_library_initialization_level(MODULE_INITIALIZATION_LEVEL_SCENE);
	

	return init_obj.init();
}
}
