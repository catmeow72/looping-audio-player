/*************************************************************************/
/*  editor_themes.cpp                                                    */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           GODOT ENGINE                                */
/*                      https://godotengine.org                          */
/*************************************************************************/
/* Copyright (c) 2007-2022 Juan Linietsky, Ariel Manzur.                 */
/* Copyright (c) 2014-2022 Godot Engine contributors (cf. AUTHORS.md).   */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "editor_themes.h"
#include <godot_cpp/core/object.hpp>

#include <godot_cpp/core/error_macros.hpp>
#include <godot_cpp/classes/resource_loader.hpp>
#include <godot_cpp/classes/style_box_texture.hpp>
#include <godot_cpp/classes/style_box_flat.hpp>
#include <godot_cpp/classes/style_box_empty.hpp>
#include <godot_cpp/classes/style_box_line.hpp>
#include <godot_cpp/classes/font.hpp>
#include <godot_cpp/classes/image_texture.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/theme_db.hpp>

Ref<StyleBoxTexture> CustomTheme::make_stylebox(Ref<Texture2D> p_texture, float p_left, float p_top, float p_right, float p_bottom, float p_margin_left, float p_margin_top, float p_margin_right, float p_margin_bottom, bool p_draw_center) {
	const float EDSCALE = get_default_base_scale();
	Ref<StyleBoxTexture> style(memnew(StyleBoxTexture));
	style->set_texture(p_texture);
	style->set_texture_margin(Side::SIDE_LEFT, p_left * EDSCALE);
	style->set_texture_margin(Side::SIDE_TOP, p_top * EDSCALE);
	style->set_texture_margin(Side::SIDE_RIGHT, p_right * EDSCALE);
	style->set_texture_margin(Side::SIDE_BOTTOM, p_bottom * EDSCALE);
	style->set_content_margin(Side::SIDE_LEFT, p_margin_left * EDSCALE);
	style->set_content_margin(Side::SIDE_TOP, p_margin_top * EDSCALE);
	style->set_content_margin(Side::SIDE_RIGHT, p_margin_right * EDSCALE);
	style->set_content_margin(Side::SIDE_BOTTOM, p_margin_bottom * EDSCALE);
	style->set_draw_center(p_draw_center);
	return style;
}

Ref<StyleBoxFlat> CustomTheme::make_circle(Ref<StyleBoxFlat> p_style) {
	Ref<StyleBoxFlat> style = p_style->duplicate();
	style->set_anti_aliased(true);
	style->set_corner_radius_all(512);
	return style;
}

Ref<StyleBoxEmpty> CustomTheme::make_empty_stylebox(float p_margin_left, float p_margin_top, float p_margin_right, float p_margin_bottom) {
	const float EDSCALE = get_default_base_scale();
	Ref<StyleBoxEmpty> style(memnew(StyleBoxEmpty));
	style->set_content_margin(Side::SIDE_LEFT, p_margin_left * EDSCALE);
	style->set_content_margin(Side::SIDE_TOP, p_margin_top * EDSCALE);
	style->set_content_margin(Side::SIDE_RIGHT, p_margin_right * EDSCALE);
	style->set_content_margin(Side::SIDE_BOTTOM, p_margin_bottom * EDSCALE);
	return style;
}

Ref<StyleBoxFlat> CustomTheme::make_flat_stylebox(Color p_color, float p_margin_left, float p_margin_top, float p_margin_right, float p_margin_bottom, int p_corner_width) {
	const float EDSCALE = get_default_base_scale();
	Ref<StyleBoxFlat> style(memnew(StyleBoxFlat));

	style->set_bg_color(p_color);
	// Adjust level of detail based on the corners' effective sizes.
	style->set_corner_detail(Math::ceil(0.8 * p_corner_width * EDSCALE));
	style->set_corner_radius_all(p_corner_width * EDSCALE);
	style->set_content_margin(Side::SIDE_LEFT, p_margin_left * EDSCALE);
	style->set_content_margin(Side::SIDE_TOP, p_margin_top * EDSCALE);
	style->set_content_margin(Side::SIDE_RIGHT, p_margin_right * EDSCALE);
	style->set_content_margin(Side::SIDE_BOTTOM, p_margin_bottom * EDSCALE);
	// Work around issue about antialiased edges being blurrier (GH-35279).
	style->set_anti_aliased(false);

	return style;
}

Ref<StyleBoxLine> CustomTheme::make_line_stylebox(Color p_color, int p_thickness, float p_grow_begin, float p_grow_end, bool p_vertical) {
	Ref<StyleBoxLine> style(memnew(StyleBoxLine));
	style->set_color(p_color);
	style->set_grow_begin(p_grow_begin);
	style->set_grow_end(p_grow_end);
	style->set_thickness(p_thickness);
	style->set_vertical(p_vertical);
	return style;
}

bool CustomTheme::is_dark_theme() const {
	Color base_color = get_base_color();
	return base_color.get_luminance() < 0.5;
}

void CustomTheme::convert_dark_theme(bool dark) {
	if (dark != is_dark_theme()) {
		Color base_color = get_base_color();
		float value = base_color.get_v();
		value = 1.0 - value;
		base_color.set_v(value);
		set_base_color(base_color);
		float contrast = get_contrast();
		contrast *= -1;
		set_contrast(contrast);
		update();
	}
}

void CustomTheme::update_icon(StringName name, StringName type, StringName from_name, StringName from_type) {
	if (base->has_icon("from_name", "from_type")) {
		Ref<Texture2D> icon = base->get_icon(from_name, from_type);
		set_icon(name, type, icon);
	}
}

void CustomTheme::update() {
	clear();
	if (base.is_null()) {
		base = ThemeDB::get_singleton()->get_default_theme();
	}
	const float EDSCALE = get_scale();
	// Controls may rely on the scale for their internal drawing logic.
	set_default_base_scale(EDSCALE);

	// Theme settings
	Color accent_color = get_accent_color();
	Color base_color = get_base_color();
	float contrast = get_contrast();
	float relationship_line_opacity = get_relationship_line_opacity();

	int border_size = get_border_size();
	int corner_radius = get_corner_radius();

	// Colors
	bool dark_theme = is_dark_theme();

	// Ensure base colors are in the 0..1 luminance range to avoid 8-bit integer overflow or text rendering issues.
	// Some places in the editor use 8-bit integer colors.
	const Color dark_color_1 = base_color.lerp(Color(0, 0, 0, 1), contrast).clamp();
	const Color dark_color_2 = base_color.lerp(Color(0, 0, 0, 1), contrast * 1.5).clamp();
	const Color dark_color_3 = base_color.lerp(Color(0, 0, 0, 1), contrast * 2).clamp();

	const Color background_color = dark_color_2;

	// White (dark theme) or black (light theme), will be used to generate the rest of the colors
	const Color mono_color = dark_theme ? Color(1, 1, 1) : Color(0, 0, 0);

	const Color contrast_color_1 = base_color.lerp(mono_color, contrast);
	const Color contrast_color_2 = base_color.lerp(mono_color, contrast * 1.5);

	const Color font_color = mono_color.lerp(base_color, 0.25);
	const Color font_hover_color = mono_color.lerp(base_color, 0.125);
	const Color font_focus_color = mono_color.lerp(base_color, 0.125);
	const Color font_hover_pressed_color = font_hover_color.lerp(accent_color, 0.74);
	const Color font_disabled_color = Color(mono_color.r, mono_color.g, mono_color.b, 0.3);
	const Color font_readonly_color = Color(mono_color.r, mono_color.g, mono_color.b, 0.65);
	const Color font_placeholder_color = Color(mono_color.r, mono_color.g, mono_color.b, 0.6);
	const Color selection_color = accent_color * Color(1, 1, 1, 0.4);
	const Color disabled_color = mono_color.inverted().lerp(base_color, 0.7);
	const Color disabled_bg_color = mono_color.inverted().lerp(base_color, 0.9);

	const Color icon_normal_color = Color(1, 1, 1);
	Color icon_hover_color = icon_normal_color * (dark_theme ? 1.15 : 1.45);
	icon_hover_color.a = 1.0;
	Color icon_focus_color = icon_hover_color;
	Color icon_disabled_color = Color(icon_normal_color, 0.4);
	// Make the pressed icon color overbright because icons are not completely white on a dark theme.
	// On a light theme, icons are dark, so we need to modulate them with an even brighter color.
	Color icon_pressed_color = accent_color * (dark_theme ? 1.15 : 3.5);
	icon_pressed_color.a = 1.0;

	const Color separator_color = Color(mono_color.r, mono_color.g, mono_color.b, 0.1);
	const Color highlight_color = Color(accent_color.r, accent_color.g, accent_color.b, 0.275);
	const Color disabled_highlight_color = highlight_color.lerp(dark_theme ? Color(0, 0, 0) : Color(1, 1, 1), 0.5);

	// Can't save single float in theme, so using Color.
	//set_color("icon_saturation", "Editor", Color(icon_saturation, icon_saturation, icon_saturation));
	set_color("accent_color", "Editor", accent_color);
	set_color("highlight_color", "Editor", highlight_color);
	set_color("disabled_highlight_color", "Editor", disabled_highlight_color);
	set_color("base_color", "Editor", base_color);
	set_color("dark_color_1", "Editor", dark_color_1);
	set_color("dark_color_2", "Editor", dark_color_2);
	set_color("dark_color_3", "Editor", dark_color_3);
	set_color("contrast_color_1", "Editor", contrast_color_1);
	set_color("contrast_color_2", "Editor", contrast_color_2);
	set_color("box_selection_fill_color", "Editor", accent_color * Color(1, 1, 1, 0.3));
	set_color("box_selection_stroke_color", "Editor", accent_color * Color(1, 1, 1, 0.8));

	set_color("axis_x_color", "Editor", Color(0.96, 0.20, 0.32));
	set_color("axis_y_color", "Editor", Color(0.53, 0.84, 0.01));
	set_color("axis_z_color", "Editor", Color(0.16, 0.55, 0.96));
	set_color("axis_w_color", "Editor", Color(0.55, 0.55, 0.55));

	const float prop_color_saturation = accent_color.get_s() * 0.75;
	const float prop_color_value = accent_color.get_v();

	set_color("property_color_x", "Editor", Color().from_hsv(0.0 / 3.0 + 0.05, prop_color_saturation, prop_color_value));
	set_color("property_color_y", "Editor", Color().from_hsv(1.0 / 3.0 + 0.05, prop_color_saturation, prop_color_value));
	set_color("property_color_z", "Editor", Color().from_hsv(2.0 / 3.0 + 0.05, prop_color_saturation, prop_color_value));
	set_color("property_color_w", "Editor", Color().from_hsv(1.5 / 3.0 + 0.05, prop_color_saturation, prop_color_value));

	set_color("font_color", "Editor", font_color);
	set_color("highlighted_font_color", "Editor", font_hover_color);
	set_color("disabled_font_color", "Editor", font_disabled_color);
	set_color("readonly_font_color", "Editor", font_readonly_color);

	set_color("mono_color", "Editor", mono_color);

	Color success_color = Color(0.45, 0.95, 0.5);
	Color warning_color = Color(1, 0.87, 0.4);
	Color error_color = Color(1, 0.47, 0.42);
	Color property_color = font_color.lerp(Color(0.5, 0.5, 0.5), 0.5);
	Color readonly_color = property_color.lerp(dark_theme ? Color(0, 0, 0) : Color(1, 1, 1), 0.25);
	Color readonly_warning_color = error_color.lerp(dark_theme ? Color(0, 0, 0) : Color(1, 1, 1), 0.25);

	if (!dark_theme) {
		// Darken some colors to be readable on a light background.
		success_color = success_color.lerp(mono_color, 0.35);
		warning_color = warning_color.lerp(mono_color, 0.35);
		error_color = error_color.lerp(mono_color, 0.25);
	}

	set_color("success_color", "Editor", success_color);
	set_color("warning_color", "Editor", warning_color);
	set_color("error_color", "Editor", error_color);
	set_color("property_color", "Editor", property_color);
	set_color("readonly_color", "Editor", readonly_color);

	if (!dark_theme) {
		set_color("highend_color", "Editor", Color::hex(0xad1128ff));
	} else {
		set_color("highend_color", "Editor", Color(1.0, 0.0, 0.0));
	}
	const int thumb_size = 24;
	set_constant("scale", "Editor", EDSCALE);
	set_constant("thumb_size", "Editor", thumb_size);
	set_constant("dark_theme", "Editor", dark_theme);
	set_constant("color_picker_button_height", "Editor", 28 * EDSCALE);

	// Ensure borders are visible when using an editor scale below 100%.
	const int border_width = CLAMP(border_size, 0, 2) * MAX(1, EDSCALE);
	const int corner_width = CLAMP(corner_radius, 0, 6);
	const int default_margin_size = 4;
	const int margin_size_extra = default_margin_size + CLAMP(border_size, 0, 2);

	// Styleboxes
	// This is the most commonly used stylebox, variations should be made as duplicate of this
	Ref<StyleBoxFlat> style_default = make_flat_stylebox(base_color, default_margin_size, default_margin_size, default_margin_size, default_margin_size, corner_width);
	style_default->set_border_width_all(border_width);
	style_default->set_border_color(base_color);

	// Button and widgets
	const float extra_spacing = get_spacing();

	const Vector2 widget_default_margin = Vector2(extra_spacing + 6, extra_spacing + default_margin_size + 1) * EDSCALE;

	Ref<StyleBoxFlat> style_widget = style_default->duplicate();
	style_widget->set_content_margin(Side::SIDE_LEFT, widget_default_margin.x);
	style_widget->set_content_margin(Side::SIDE_TOP, widget_default_margin.y);
	style_widget->set_content_margin(Side::SIDE_RIGHT, widget_default_margin.x);
	style_widget->set_content_margin(Side::SIDE_BOTTOM, widget_default_margin.y);
	style_widget->set_bg_color(dark_color_1);
	style_widget->set_border_color(dark_color_2);

	Ref<StyleBoxFlat> style_widget_disabled = style_widget->duplicate();
	style_widget_disabled->set_border_color(disabled_color);
	style_widget_disabled->set_bg_color(disabled_bg_color);

	Ref<StyleBoxFlat> style_widget_focus = style_widget->duplicate();
	style_widget_focus->set_draw_center(false);
	style_widget_focus->set_border_width_all(Math::round(2 * MAX(1, EDSCALE)));
	style_widget_focus->set_border_color(accent_color);

	Color pressed_bg_color = dark_color_1.darkened(0.125);
	Ref<StyleBoxFlat> style_widget_pressed = style_widget->duplicate();
	style_widget_pressed->set_bg_color(pressed_bg_color);

	Color hover_bg_color = mono_color * Color(1, 1, 1, 0.11);
	Ref<StyleBoxFlat> style_widget_hover = style_widget->duplicate();
	style_widget_hover->set_bg_color(hover_bg_color);
	style_widget_hover->set_border_color(mono_color * Color(1, 1, 1, 0.05));

	// Style for windows, popups, etc..
	Ref<StyleBoxFlat> style_popup = style_default->duplicate();
	const int popup_margin_size = default_margin_size * EDSCALE * 3;
	style_popup->set_content_margin_all(popup_margin_size);
	style_popup->set_border_color(contrast_color_1);
	const Color shadow_color = Color(0, 0, 0, dark_theme ? 0.3 : 0.1);
	style_popup->set_shadow_color(shadow_color);
	style_popup->set_shadow_size(4 * EDSCALE);
	// Popups are separate windows by default in the editor. Windows currently don't support per-pixel transparency
	// in 4.0, and even if it was, it may not always work in practice (e.g. running with compositing disabled).
	style_popup->set_corner_radius_all(0);

	Ref<StyleBoxLine> style_popup_separator(memnew(StyleBoxLine));
	style_popup_separator->set_color(separator_color);
	style_popup_separator->set_grow_begin(popup_margin_size - MAX(Math::round(EDSCALE), border_width));
	style_popup_separator->set_grow_end(popup_margin_size - MAX(Math::round(EDSCALE), border_width));
	style_popup_separator->set_thickness(MAX(Math::round(EDSCALE), border_width));

	Ref<StyleBoxLine> style_popup_labeled_separator_left(memnew(StyleBoxLine));
	style_popup_labeled_separator_left->set_grow_begin(popup_margin_size - MAX(Math::round(EDSCALE), border_width));
	style_popup_labeled_separator_left->set_color(separator_color);
	style_popup_labeled_separator_left->set_thickness(MAX(Math::round(EDSCALE), border_width));

	Ref<StyleBoxLine> style_popup_labeled_separator_right(memnew(StyleBoxLine));
	style_popup_labeled_separator_right->set_grow_end(popup_margin_size - MAX(Math::round(EDSCALE), border_width));
	style_popup_labeled_separator_right->set_color(separator_color);
	style_popup_labeled_separator_right->set_thickness(MAX(Math::round(EDSCALE), border_width));

	Ref<StyleBoxEmpty> style_empty = make_empty_stylebox(default_margin_size, default_margin_size, default_margin_size, default_margin_size);

	// TabBar

	Ref<StyleBoxFlat> style_tab_base = style_widget->duplicate();

	style_tab_base->set_border_width_all(0);
	// Don't round the top corners to avoid creating a small blank space between the tabs and the main panel.
	// This also makes the top highlight look better.
	style_tab_base->set_corner_radius(CORNER_BOTTOM_LEFT, 0);
	style_tab_base->set_corner_radius(CORNER_BOTTOM_RIGHT, 0);

	// When using a border width greater than 0, visually line up the left of the selected tab with the underlying panel.
	style_tab_base->set_expand_margin(SIDE_LEFT, -border_width);

	style_tab_base->set_content_margin(SIDE_LEFT, widget_default_margin.x + 5 * EDSCALE);
	style_tab_base->set_content_margin(SIDE_RIGHT, widget_default_margin.x + 5 * EDSCALE);
	style_tab_base->set_content_margin(SIDE_BOTTOM, widget_default_margin.y);
	style_tab_base->set_content_margin(SIDE_TOP, widget_default_margin.y);

	Ref<StyleBoxFlat> style_tab_selected = style_tab_base->duplicate();

	style_tab_selected->set_bg_color(base_color);
	// Add a highlight line at the top of the selected tab.
	style_tab_selected->set_border_width(SIDE_TOP, Math::round(2 * EDSCALE));
	// Make the highlight line prominent, but not too prominent as to not be distracting.
	Color tab_highlight = dark_color_2.lerp(accent_color, 0.75);
	style_tab_selected->set_border_color(tab_highlight);
	style_tab_selected->set_corner_radius_all(0);

	Ref<StyleBoxFlat> style_tab_unselected = style_tab_base->duplicate();
	style_tab_unselected->set_expand_margin(SIDE_BOTTOM, 0);
	style_tab_unselected->set_bg_color(dark_color_1);
	// Add some spacing between unselected tabs to make them easier to distinguish from each other
	style_tab_unselected->set_border_color(Color(0, 0, 0, 0));

	Ref<StyleBoxFlat> style_tab_disabled = style_tab_base->duplicate();
	style_tab_disabled->set_expand_margin(SIDE_BOTTOM, 0);
	style_tab_disabled->set_bg_color(disabled_bg_color);
	style_tab_disabled->set_border_color(disabled_bg_color);

	// Editor background
	Color background_color_opaque = background_color;
	background_color_opaque.a = 1.0;
	set_stylebox("Background", "EditorStyles", make_flat_stylebox(background_color_opaque, default_margin_size, default_margin_size, default_margin_size, default_margin_size));

	// Focus
	set_stylebox("Focus", "EditorStyles", style_widget_focus);
	// Use a less opaque color to be less distracting for the 2D and 3D editor viewports.
	Ref<StyleBoxFlat> style_widget_focus_viewport = style_widget_focus->duplicate();
	style_widget_focus_viewport->set_border_color(accent_color * Color(1, 1, 1, 0.5));
	set_stylebox("FocusViewport", "EditorStyles", style_widget_focus_viewport);

	// Menu
	Ref<StyleBoxFlat> style_menu = style_widget->duplicate();
	style_menu->set_draw_center(false);
	style_menu->set_border_width_all(0);
	set_stylebox("panel", "PanelContainer", style_menu);
	set_stylebox("MenuPanel", "EditorStyles", style_menu);

	// CanvasItem Editor
	Ref<StyleBoxFlat> style_canvas_editor_info = make_flat_stylebox(Color(0.0, 0.0, 0.0, 0.2));
	style_canvas_editor_info->set_expand_margin_all(4 * EDSCALE);
	set_stylebox("CanvasItemInfoOverlay", "EditorStyles", style_canvas_editor_info);

	// 2D and 3D contextual toolbar.
	// Use a custom stylebox to make contextual menu items stand out from the rest.
	// This helps with editor usability as contextual menu items change when selecting nodes,
	// even though it may not be immediately obvious at first.
	Ref<StyleBoxFlat> toolbar_stylebox_base = memnew(StyleBoxFlat);
	toolbar_stylebox_base->set_bg_color(accent_color * Color(1, 1, 1, 0.1));
	toolbar_stylebox_base->set_corner_radius(CORNER_TOP_LEFT, corner_radius * EDSCALE);
	toolbar_stylebox_base->set_corner_radius(CORNER_TOP_RIGHT, corner_radius * EDSCALE);
	toolbar_stylebox_base->set_anti_aliased(false);
	// Add an underline to the StyleBox, but prevent its minimum vertical size from changing.
	toolbar_stylebox_base->set_border_color(accent_color);
	toolbar_stylebox_base->set_border_width(SIDE_BOTTOM, Math::round(2 * EDSCALE));
	toolbar_stylebox_base->set_content_margin(SIDE_BOTTOM, 0);
	Ref<StyleBoxFlat> toolbar_stylebox = memnew(StyleBoxFlat);
	toolbar_stylebox->set_bg_color(accent_color);
	toolbar_stylebox->set_border_color(accent_color);
	set_stylebox("ContextualToolbar", "EditorStyles", toolbar_stylebox);

	// Script Editor
	set_stylebox("ScriptEditorPanel", "EditorStyles", make_empty_stylebox(default_margin_size, 0, default_margin_size, default_margin_size));
	set_stylebox("ScriptEditor", "EditorStyles", make_empty_stylebox(0, 0, 0, 0));

	// Launch Pad and Play buttons
	Ref<StyleBoxFlat> style_launch_pad = make_flat_stylebox(dark_color_1, 2 * EDSCALE, 0, 2 * EDSCALE, 0, corner_width);
	style_launch_pad->set_corner_radius_all(corner_radius * EDSCALE);
	set_stylebox("LaunchPadNormal", "EditorStyles", style_launch_pad);
	Ref<StyleBoxFlat> style_launch_pad_movie = style_launch_pad->duplicate();
	style_launch_pad_movie->set_bg_color(accent_color * Color(1, 1, 1, 0.1));
	style_launch_pad_movie->set_border_color(accent_color);
	style_launch_pad_movie->set_border_width_all(Math::round(2 * EDSCALE));
	set_stylebox("LaunchPadMovieMode", "EditorStyles", style_launch_pad_movie);

	set_stylebox("MovieWriterButtonNormal", "EditorStyles", make_empty_stylebox(0, 0, 0, 0));
	Ref<StyleBoxFlat> style_write_movie_button = style_widget_pressed->duplicate();
	style_write_movie_button->set_bg_color(accent_color);
	style_write_movie_button->set_corner_radius_all(corner_radius * EDSCALE);
	style_write_movie_button->set_content_margin(SIDE_TOP, 0);
	style_write_movie_button->set_content_margin(SIDE_BOTTOM, 0);
	style_write_movie_button->set_content_margin(SIDE_LEFT, 0);
	style_write_movie_button->set_content_margin(SIDE_RIGHT, 0);
	style_write_movie_button->set_expand_margin(SIDE_RIGHT, 2 * EDSCALE);
	set_stylebox("MovieWriterButtonPressed", "EditorStyles", style_write_movie_button);

	set_stylebox("normal", "MenuButton", style_menu);
	set_stylebox("hover", "MenuButton", style_widget_hover);
	set_stylebox("pressed", "MenuButton", style_menu);
	set_stylebox("focus", "MenuButton", style_menu);
	set_stylebox("disabled", "MenuButton", style_menu);

	set_color("font_color", "MenuButton", font_color);
	set_color("font_hover_color", "MenuButton", font_hover_color);
	set_color("font_hover_pressed_color", "MenuButton", font_hover_pressed_color);
	set_color("font_focus_color", "MenuButton", font_focus_color);

	set_stylebox("MenuHover", "EditorStyles", style_widget_hover);

	// Buttons
	set_stylebox("normal", "Button", style_widget);
	set_stylebox("hover", "Button", style_widget_hover);
	set_stylebox("pressed", "Button", style_widget_pressed);
	set_stylebox("focus", "Button", style_widget_focus);
	set_stylebox("disabled", "Button", style_widget_disabled);

	set_color("font_color", "Button", font_color);
	set_color("font_hover_color", "Button", font_hover_color);
	set_color("font_hover_pressed_color", "Button", font_hover_pressed_color);
	set_color("font_focus_color", "Button", font_focus_color);
	set_color("font_pressed_color", "Button", accent_color);
	set_color("font_disabled_color", "Button", font_disabled_color);

	set_color("icon_normal_color", "Button", icon_normal_color);
	set_color("icon_hover_color", "Button", icon_hover_color);
	set_color("icon_focus_color", "Button", icon_focus_color);
	set_color("icon_pressed_color", "Button", icon_pressed_color);
	set_color("icon_disabled_color", "Button", icon_disabled_color);

	const float ACTION_BUTTON_EXTRA_MARGIN = 32 * EDSCALE;

	set_type_variation("InspectorActionButton", "Button");
	Color color_inspector_action = dark_color_1.lerp(mono_color, 0.12);
	color_inspector_action.a = 0.5;
	Ref<StyleBoxFlat> style_inspector_action = style_widget->duplicate();
	style_inspector_action->set_bg_color(color_inspector_action);
	style_inspector_action->set_content_margin(SIDE_RIGHT, ACTION_BUTTON_EXTRA_MARGIN);
	set_stylebox("normal", "InspectorActionButton", style_inspector_action);
	style_inspector_action = style_widget_hover->duplicate();
	style_inspector_action->set_content_margin(SIDE_RIGHT, ACTION_BUTTON_EXTRA_MARGIN);
	set_stylebox("hover", "InspectorActionButton", style_inspector_action);
	style_inspector_action = style_widget_pressed->duplicate();
	style_inspector_action->set_content_margin(SIDE_RIGHT, ACTION_BUTTON_EXTRA_MARGIN);
	set_stylebox("pressed", "InspectorActionButton", style_inspector_action);
	style_inspector_action = style_widget_disabled->duplicate();
	style_inspector_action->set_content_margin(SIDE_RIGHT, ACTION_BUTTON_EXTRA_MARGIN);
	set_stylebox("disabled", "InspectorActionButton", style_inspector_action);
	set_constant("h_separation", "InspectorActionButton", ACTION_BUTTON_EXTRA_MARGIN);

	// Variation for Editor Log filter buttons
	set_type_variation("EditorLogFilterButton", "Button");
	// When pressed, don't tint the icons with the accent color, just leave them normal.
	set_color("icon_pressed_color", "EditorLogFilterButton", icon_normal_color);
	// When unpressed, dim the icons.
	set_color("icon_normal_color", "EditorLogFilterButton", icon_disabled_color);
	// When pressed, add a small bottom border to the buttons to better show their active state,
	// similar to active tabs.
	Ref<StyleBoxFlat> editor_log_button_pressed = style_widget_pressed->duplicate();
	editor_log_button_pressed->set_border_width(SIDE_BOTTOM, 2 * EDSCALE);
	editor_log_button_pressed->set_border_color(accent_color);
	set_stylebox("pressed", "EditorLogFilterButton", editor_log_button_pressed);

	// MenuBar
	set_stylebox("normal", "MenuBar", style_widget);
	set_stylebox("hover", "MenuBar", style_widget_hover);
	set_stylebox("pressed", "MenuBar", style_widget_pressed);
	set_stylebox("focus", "MenuBar", style_widget_focus);
	set_stylebox("disabled", "MenuBar", style_widget_disabled);

	set_color("font_color", "MenuBar", font_color);
	set_color("font_hover_color", "MenuBar", font_hover_color);
	set_color("font_hover_pressed_color", "MenuBar", font_hover_pressed_color);
	set_color("font_focus_color", "MenuBar", font_focus_color);
	set_color("font_pressed_color", "MenuBar", accent_color);
	set_color("font_disabled_color", "MenuBar", font_disabled_color);

	set_color("icon_normal_color", "MenuBar", icon_normal_color);
	set_color("icon_hover_color", "MenuBar", icon_hover_color);
	set_color("icon_focus_color", "MenuBar", icon_focus_color);
	set_color("icon_pressed_color", "MenuBar", icon_pressed_color);
	set_color("icon_disabled_color", "MenuBar", icon_disabled_color);

	// OptionButton
	Ref<StyleBoxFlat> style_option_button_focus = style_widget_focus->duplicate();
	Ref<StyleBoxFlat> style_option_button_normal = style_widget->duplicate();
	Ref<StyleBoxFlat> style_option_button_hover = style_widget_hover->duplicate();
	Ref<StyleBoxFlat> style_option_button_pressed = style_widget_pressed->duplicate();
	Ref<StyleBoxFlat> style_option_button_disabled = style_widget_disabled->duplicate();

	style_option_button_focus->set_content_margin(SIDE_RIGHT, 4 * EDSCALE);
	style_option_button_normal->set_content_margin(SIDE_RIGHT, 4 * EDSCALE);
	style_option_button_hover->set_content_margin(SIDE_RIGHT, 4 * EDSCALE);
	style_option_button_pressed->set_content_margin(SIDE_RIGHT, 4 * EDSCALE);
	style_option_button_disabled->set_content_margin(SIDE_RIGHT, 4 * EDSCALE);

	set_stylebox("focus", "OptionButton", style_option_button_focus);
	set_stylebox("normal", "OptionButton", style_widget);
	set_stylebox("hover", "OptionButton", style_widget_hover);
	set_stylebox("pressed", "OptionButton", style_widget_pressed);
	set_stylebox("disabled", "OptionButton", style_widget_disabled);

	set_stylebox("normal_mirrored", "OptionButton", style_option_button_normal);
	set_stylebox("hover_mirrored", "OptionButton", style_option_button_hover);
	set_stylebox("pressed_mirrored", "OptionButton", style_option_button_pressed);
	set_stylebox("disabled_mirrored", "OptionButton", style_option_button_disabled);

	set_color("font_color", "OptionButton", font_color);
	set_color("font_hover_color", "OptionButton", font_hover_color);
	set_color("font_hover_pressed_color", "OptionButton", font_hover_pressed_color);
	set_color("font_focus_color", "OptionButton", font_focus_color);
	set_color("font_pressed_color", "OptionButton", accent_color);
	set_color("font_disabled_color", "OptionButton", font_disabled_color);

	set_color("icon_normal_color", "OptionButton", icon_normal_color);
	set_color("icon_hover_color", "OptionButton", icon_hover_color);
	set_color("icon_focus_color", "OptionButton", icon_focus_color);
	set_color("icon_pressed_color", "OptionButton", icon_pressed_color);
	set_color("icon_disabled_color", "OptionButton", icon_disabled_color);

	update_icon("arrow", "OptionButton", SNAME("GuiOptionArrow"), SNAME("EditorIcons"));
	set_constant("arrow_margin", "OptionButton", widget_default_margin.x - 2 * EDSCALE);
	set_constant("modulate_arrow", "OptionButton", true);
	set_constant("h_separation", "OptionButton", 4 * EDSCALE);

	// CheckButton
	set_stylebox("normal", "CheckButton", style_menu);
	set_stylebox("pressed", "CheckButton", style_menu);
	set_stylebox("disabled", "CheckButton", style_menu);
	set_stylebox("hover", "CheckButton", style_menu);
	set_stylebox("hover_pressed", "CheckButton", style_menu);

	update_icon("checked", "CheckButton", SNAME("GuiToggleOn"), SNAME("EditorIcons"));
	update_icon("checked_disabled", "CheckButton", SNAME("GuiToggleOnDisabled"), SNAME("EditorIcons"));
	update_icon("unchecked", "CheckButton", SNAME("GuiToggleOff"), SNAME("EditorIcons"));
	update_icon("unchecked_disabled", "CheckButton", SNAME("GuiToggleOffDisabled"), SNAME("EditorIcons"));

	update_icon("checked_mirrored", "CheckButton", SNAME("GuiToggleOnMirrored"), SNAME("EditorIcons"));
	update_icon("checked_disabled_mirrored", "CheckButton", SNAME("GuiToggleOnDisabledMirrored"), SNAME("EditorIcons"));
	update_icon("unchecked_mirrored", "CheckButton", SNAME("GuiToggleOffMirrored"), SNAME("EditorIcons"));
	update_icon("unchecked_disabled_mirrored", "CheckButton", SNAME("GuiToggleOffDisabledMirrored"), SNAME("EditorIcons"));
	
	set_color("font_color", "CheckButton", font_color);
	set_color("font_hover_color", "CheckButton", font_hover_color);
	set_color("font_hover_pressed_color", "CheckButton", font_hover_pressed_color);
	set_color("font_focus_color", "CheckButton", font_focus_color);
	set_color("font_pressed_color", "CheckButton", accent_color);
	set_color("font_disabled_color", "CheckButton", font_disabled_color);

	set_color("icon_normal_color", "CheckButton", icon_normal_color);
	set_color("icon_hover_color", "CheckButton", icon_hover_color);
	set_color("icon_focus_color", "CheckButton", icon_focus_color);
	set_color("icon_pressed_color", "CheckButton", icon_pressed_color);
	set_color("icon_disabled_color", "CheckButton", icon_disabled_color);

	set_constant("h_separation", "CheckButton", 8 * EDSCALE);
	set_constant("check_v_offset", "CheckButton", 0 * EDSCALE);

	// Checkbox
	Ref<StyleBoxFlat> sb_checkbox = style_menu->duplicate();
	sb_checkbox->set_content_margin_all(default_margin_size * EDSCALE);

	set_stylebox("normal", "CheckBox", sb_checkbox);
	set_stylebox("pressed", "CheckBox", sb_checkbox);
	set_stylebox("disabled", "CheckBox", sb_checkbox);
	set_stylebox("hover", "CheckBox", sb_checkbox);
	set_stylebox("hover_pressed", "CheckBox", sb_checkbox);

	update_icon("checked", "CheckBox", SNAME("GuiChecked"), SNAME("EditorIcons"));
	update_icon("unchecked", "CheckBox", SNAME("GuiUnchecked"), SNAME("EditorIcons"));
	update_icon("radio_checked", "CheckBox", SNAME("GuiRadioChecked"), SNAME("EditorIcons"));
	update_icon("radio_unchecked", "CheckBox", SNAME("GuiRadioUnchecked"), SNAME("EditorIcons"));
	update_icon("checked_disabled", "CheckBox", SNAME("GuiCheckedDisabled"), SNAME("EditorIcons"));
	update_icon("unchecked_disabled", "CheckBox", SNAME("GuiUncheckedDisabled"), SNAME("EditorIcons"));
	update_icon("radio_checked_disabled", "CheckBox", SNAME("GuiRadioCheckedDisabled"), SNAME("EditorIcons"));
	update_icon("radio_unchecked_disabled", "CheckBox", SNAME("GuiRadioUncheckedDisabled"), SNAME("EditorIcons"));

	set_color("font_color", "CheckBox", font_color);
	set_color("font_hover_color", "CheckBox", font_hover_color);
	set_color("font_hover_pressed_color", "CheckBox", font_hover_pressed_color);
	set_color("font_focus_color", "CheckBox", font_focus_color);
	set_color("font_pressed_color", "CheckBox", accent_color);
	set_color("font_disabled_color", "CheckBox", font_disabled_color);

	set_color("icon_normal_color", "CheckBox", icon_normal_color);
	set_color("icon_hover_color", "CheckBox", icon_hover_color);
	set_color("icon_focus_color", "CheckBox", icon_focus_color);
	set_color("icon_pressed_color", "CheckBox", icon_pressed_color);
	set_color("icon_disabled_color", "CheckBox", icon_disabled_color);

	set_constant("h_separation", "CheckBox", 8 * EDSCALE);
	set_constant("check_v_offset", "CheckBox", 0 * EDSCALE);

	// PopupDialog
	set_stylebox("panel", "PopupDialog", style_popup);

	// PopupMenu
	Ref<StyleBoxFlat> style_popup_menu = style_popup->duplicate();
	// Use 1 pixel for the sides, since if 0 is used, the highlight of hovered items is drawn
	// on top of the popup border. This causes a 'gap' in the panel border when an item is highlighted,
	// and it looks weird. 1px solves this.
	style_popup_menu->set_content_margin(Side::SIDE_LEFT, EDSCALE);
	style_popup_menu->set_content_margin(Side::SIDE_TOP, 2 * EDSCALE);
	style_popup_menu->set_content_margin(Side::SIDE_RIGHT, EDSCALE);
	style_popup_menu->set_content_margin(Side::SIDE_BOTTOM, 2 * EDSCALE);
	// Always display a border for PopupMenus so they can be distinguished from their background.
	style_popup_menu->set_border_width_all(EDSCALE);
	style_popup_menu->set_border_color(dark_color_2);
	set_stylebox("panel", "PopupMenu", style_popup_menu);

	Ref<StyleBoxFlat> style_menu_hover = style_widget_hover->duplicate();
	// Don't use rounded corners for hover highlights since the StyleBox touches the PopupMenu's edges.
	style_menu_hover->set_corner_radius_all(0);
	set_stylebox("hover", "PopupMenu", style_menu_hover);

	set_stylebox("separator", "PopupMenu", style_popup_separator);
	set_stylebox("labeled_separator_left", "PopupMenu", style_popup_labeled_separator_left);
	set_stylebox("labeled_separator_right", "PopupMenu", style_popup_labeled_separator_right);

	set_color("font_color", "PopupMenu", font_color);
	set_color("font_hover_color", "PopupMenu", font_hover_color);
	set_color("font_accelerator_color", "PopupMenu", font_disabled_color);
	set_color("font_disabled_color", "PopupMenu", font_disabled_color);
	set_color("font_separator_color", "PopupMenu", font_disabled_color);

	update_icon("checked", "PopupMenu", SNAME("GuiChecked"), SNAME("EditorIcons"));
	update_icon("unchecked", "PopupMenu", SNAME("GuiUnchecked"), SNAME("EditorIcons"));
	update_icon("radio_checked", "PopupMenu", SNAME("GuiRadioChecked"), SNAME("EditorIcons"));
	update_icon("radio_unchecked", "PopupMenu", SNAME("GuiRadioUnchecked"), SNAME("EditorIcons"));
	update_icon("checked_disabled", "PopupMenu", SNAME("GuiCheckedDisabled"), SNAME("EditorIcons"));
	update_icon("unchecked_disabled", "PopupMenu", SNAME("GuiUncheckedDisabled"), SNAME("EditorIcons"));
	update_icon("radio_checked_disabled", "PopupMenu", SNAME("GuiRadioCheckedDisabled"), SNAME("EditorIcons"));
	update_icon("radio_unchecked_disabled", "PopupMenu", SNAME("GuiRadioUncheckedDisabled"), SNAME("EditorIcons"));
	update_icon("submenu", "PopupMenu", SNAME("ArrowRight"), SNAME("EditorIcons"));
	update_icon("submenu_mirrored", "PopupMenu", SNAME("ArrowLeft"), SNAME("EditorIcons"));
	update_icon("visibility_hidden", "PopupMenu", SNAME("GuiVisibilityHidden"), SNAME("EditorIcons"));
	update_icon("visibility_visible", "PopupMenu", SNAME("GuiVisibilityVisible"), SNAME("EditorIcons"));
	update_icon("visibility_xray", "PopupMenu", SNAME("GuiVisibilityXray"), SNAME("EditorIcons"));

	// Force the v_separation to be even so that the spacing on top and bottom is even.
	// If the vsep is odd and cannot be split into 2 even groups (of pixels), then it will be lopsided.
	// We add 2 to the vsep to give it some extra spacing which looks a bit more modern (see Windows, for example).
	const int vsep_base = extra_spacing + default_margin_size + 6;
	const int force_even_vsep = vsep_base + (vsep_base % 2);
	set_constant("v_separation", "PopupMenu", force_even_vsep * EDSCALE);
	set_constant("item_start_padding", "PopupMenu", default_margin_size * 1.5 * EDSCALE);
	set_constant("item_end_padding", "PopupMenu", default_margin_size * 1.5 * EDSCALE);
/*
	// Sub-inspectors
	for (int i = 0; i < 16; i++) {
		Color si_base_color = accent_color;

		float hue_rotate = (i * 2 % 16) / 16.0;
		si_base_color.set_hsv(Math::fmod(float(si_base_color.get_h() + hue_rotate), float(1.0)), si_base_color.get_s(), si_base_color.get_v());
		si_base_color = accent_color.lerp(si_base_color, float(EDITOR_GET("docks/property_editor/subresource_hue_tint")));

		// Sub-inspector background.
		Ref<StyleBoxFlat> sub_inspector_bg = style_default->duplicate();
		sub_inspector_bg->set_bg_color(dark_color_1.lerp(si_base_color, 0.08));
		sub_inspector_bg->set_border_width_all(2 * EDSCALE);
		sub_inspector_bg->set_border_color(si_base_color * Color(0.7, 0.7, 0.7, 0.8));
		sub_inspector_bg->set_content_margin_all(4 * EDSCALE);
		sub_inspector_bg->set_corner_radius(CORNER_TOP_LEFT, 0);
		sub_inspector_bg->set_corner_radius(CORNER_TOP_RIGHT, 0);

		set_stylebox("sub_inspector_bg" + itos(i), "Editor", sub_inspector_bg);

		// EditorProperty background while it has a sub-inspector open.
		Ref<StyleBoxFlat> bg_color = make_flat_stylebox(si_base_color * Color(0.7, 0.7, 0.7, 0.8), 0, 0, 0, 0, corner_radius);
		bg_color->set_anti_aliased(false);
		bg_color->set_corner_radius(CORNER_BOTTOM_LEFT, 0);
		bg_color->set_corner_radius(CORNER_BOTTOM_RIGHT, 0);

		set_stylebox("sub_inspector_property_bg" + itos(i), "Editor", bg_color);
	}
*/
	set_color("sub_inspector_property_color", "Editor", dark_theme ? Color(1, 1, 1, 1) : Color(0, 0, 0, 1));

	// EditorSpinSlider.
	set_color("label_color", "EditorSpinSlider", font_color);
	set_color("read_only_label_color", "EditorSpinSlider", font_readonly_color);

	Ref<StyleBoxFlat> editor_spin_label_bg = style_default->duplicate();
	editor_spin_label_bg->set_bg_color(dark_color_3);
	editor_spin_label_bg->set_border_width_all(0);
	set_stylebox("label_bg", "EditorSpinSlider", editor_spin_label_bg);

	// EditorProperty
	Ref<StyleBoxFlat> style_property_bg = style_default->duplicate();
	style_property_bg->set_bg_color(highlight_color);
	style_property_bg->set_border_width_all(0);

	Ref<StyleBoxFlat> style_property_child_bg = style_default->duplicate();
	style_property_child_bg->set_bg_color(dark_color_2);
	style_property_child_bg->set_border_width_all(0);

	set_constant("font_offset", "EditorProperty", 8 * EDSCALE);
	set_stylebox("bg_selected", "EditorProperty", style_property_bg);
	set_stylebox("bg", "EditorProperty", Ref<StyleBoxEmpty>(memnew(StyleBoxEmpty)));
	set_stylebox("child_bg", "EditorProperty", style_property_child_bg);
	set_constant("v_separation", "EditorProperty", (extra_spacing + default_margin_size) * EDSCALE);
	set_color("warning_color", "EditorProperty", warning_color);
	set_color("property_color", "EditorProperty", property_color);
	set_color("readonly_color", "EditorProperty", readonly_color);
	set_color("readonly_warning_color", "EditorProperty", readonly_warning_color);

	Ref<StyleBoxFlat> style_property_group_note = style_default->duplicate();
	Color property_group_note_color = accent_color;
	property_group_note_color.a = 0.1;
	style_property_group_note->set_bg_color(property_group_note_color);
	set_stylebox("bg_group_note", "EditorProperty", style_property_group_note);

	// EditorInspectorSection
	Color inspector_section_color = font_color.lerp(Color(0.5, 0.5, 0.5), 0.35);
	set_color("font_color", "EditorInspectorSection", inspector_section_color);

	Color inspector_indent_color = accent_color;
	inspector_indent_color.a = 0.2;
	Ref<StyleBoxFlat> inspector_indent_style = make_flat_stylebox(inspector_indent_color, 2.0 * EDSCALE, 0, 2.0 * EDSCALE, 0);
	set_stylebox("indent_box", "EditorInspectorSection", inspector_indent_style);
	set_constant("indent_size", "EditorInspectorSection", 6.0 * EDSCALE);

	set_constant("inspector_margin", "Editor", 12 * EDSCALE);

	// Tree & ItemList background
	Ref<StyleBoxFlat> style_tree_bg = style_default->duplicate();
	// Make Trees easier to distinguish from other controls by using a darker background color.
	style_tree_bg->set_bg_color(dark_color_1.lerp(dark_color_2, 0.5));
	style_tree_bg->set_border_color(dark_color_3);
	set_stylebox("panel", "Tree", style_tree_bg);

	// Tree
	update_icon("checked", "Tree", SNAME("GuiChecked"), SNAME("EditorIcons"));
	update_icon("indeterminate", "Tree", SNAME("GuiIndeterminate"), SNAME("EditorIcons"));
	update_icon("unchecked", "Tree", SNAME("GuiUnchecked"), SNAME("EditorIcons"));
	update_icon("arrow", "Tree", SNAME("GuiTreeArrowDown"), SNAME("EditorIcons"));
	update_icon("arrow_collapsed", "Tree", SNAME("GuiTreeArrowRight"), SNAME("EditorIcons"));
	update_icon("arrow_collapsed_mirrored", "Tree", SNAME("GuiTreeArrowLeft"), SNAME("EditorIcons"));
	update_icon("updown", "Tree", SNAME("GuiTreeUpdown"), SNAME("EditorIcons"));
	update_icon("select_arrow", "Tree", SNAME("GuiDropdown"), SNAME("EditorIcons"));

	set_stylebox("focus", "Tree", style_widget_focus);
	set_stylebox("custom_button", "Tree", make_empty_stylebox());
	set_stylebox("custom_button_pressed", "Tree", make_empty_stylebox());
	set_stylebox("custom_button_hover", "Tree", style_widget);
	set_color("custom_button_font_highlight", "Tree", font_hover_color);
	set_color("font_color", "Tree", font_color);
	set_color("font_selected_color", "Tree", mono_color);
	set_color("title_button_color", "Tree", font_color);
	set_color("drop_position_color", "Tree", accent_color);
	set_constant("v_separation", "Tree", widget_default_margin.y - EDSCALE);
	set_constant("h_separation", "Tree", 6 * EDSCALE);
	set_constant("guide_width", "Tree", border_width);
	set_constant("item_margin", "Tree", 3 * default_margin_size * EDSCALE);
	set_constant("button_margin", "Tree", default_margin_size * EDSCALE);
	set_constant("scroll_border", "Tree", 40 * EDSCALE);
	set_constant("scroll_speed", "Tree", 12);

	const Color guide_color = mono_color * Color(1, 1, 1, 0.05);
	Color relationship_line_color = mono_color * Color(1, 1, 1, relationship_line_opacity);

	set_constant("draw_guides", "Tree", relationship_line_opacity < 0.01);
	set_color("guide_color", "Tree", guide_color);

	int relationship_line_width = 1;
	Color parent_line_color = mono_color * Color(1, 1, 1, CLAMP(relationship_line_opacity + 0.45, 0.0, 1.0));
	Color children_line_color = mono_color * Color(1, 1, 1, CLAMP(relationship_line_opacity + 0.25, 0.0, 1.0));
	set_constant("draw_relationship_lines", "Tree", relationship_line_opacity >= 0.01);
	set_constant("relationship_line_width", "Tree", relationship_line_width);
	set_constant("parent_hl_line_width", "Tree", relationship_line_width * 2);
	set_constant("children_hl_line_width", "Tree", relationship_line_width);
	set_constant("parent_hl_line_margin", "Tree", relationship_line_width * 3);
	set_color("relationship_line_color", "Tree", relationship_line_color);
	set_color("parent_hl_line_color", "Tree", parent_line_color);
	set_color("children_hl_line_color", "Tree", children_line_color);

	Ref<StyleBoxFlat> style_tree_btn = style_default->duplicate();
	style_tree_btn->set_bg_color(highlight_color);
	style_tree_btn->set_border_width_all(0);
	set_stylebox("button_pressed", "Tree", style_tree_btn);

	Ref<StyleBoxFlat> style_tree_hover = style_default->duplicate();
	style_tree_hover->set_bg_color(highlight_color * Color(1, 1, 1, 0.4));
	style_tree_hover->set_border_width_all(0);
	set_stylebox("hover", "Tree", style_tree_hover);

	Ref<StyleBoxFlat> style_tree_focus = style_default->duplicate();
	style_tree_focus->set_bg_color(highlight_color);
	style_tree_focus->set_border_width_all(0);
	set_stylebox("selected_focus", "Tree", style_tree_focus);

	Ref<StyleBoxFlat> style_tree_selected = style_tree_focus->duplicate();
	set_stylebox("selected", "Tree", style_tree_selected);

	Ref<StyleBoxFlat> style_tree_cursor = style_default->duplicate();
	style_tree_cursor->set_draw_center(false);
	style_tree_cursor->set_border_width_all(MAX(1, border_width));
	style_tree_cursor->set_border_color(contrast_color_1);

	Ref<StyleBoxFlat> style_tree_title = style_default->duplicate();
	style_tree_title->set_bg_color(dark_color_3);
	style_tree_title->set_border_width_all(0);
	set_stylebox("cursor", "Tree", style_tree_cursor);
	set_stylebox("cursor_unfocused", "Tree", style_tree_cursor);
	set_stylebox("title_button_normal", "Tree", style_tree_title);
	set_stylebox("title_button_hover", "Tree", style_tree_title);
	set_stylebox("title_button_pressed", "Tree", style_tree_title);

	Color prop_category_color = dark_color_1.lerp(mono_color, 0.12);
	Color prop_section_color = dark_color_1.lerp(mono_color, 0.09);
	Color prop_subsection_color = dark_color_1.lerp(mono_color, 0.06);
	set_color("prop_category", "Editor", prop_category_color);
	set_color("prop_section", "Editor", prop_section_color);
	set_color("prop_subsection", "Editor", prop_subsection_color);
	set_color("drop_position_color", "Tree", accent_color);

	// EditorInspectorCategory
	Ref<StyleBoxFlat> category_bg = style_default->duplicate();
	category_bg->set_bg_color(prop_category_color);
	category_bg->set_border_color(prop_category_color);
	set_stylebox("bg", "EditorInspectorCategory", category_bg);

	// ItemList
	Ref<StyleBoxFlat> style_itemlist_bg = style_default->duplicate();
	style_itemlist_bg->set_bg_color(dark_color_1);
	style_itemlist_bg->set_border_width_all(border_width);
	style_itemlist_bg->set_border_color(dark_color_3);

	Ref<StyleBoxFlat> style_itemlist_cursor = style_default->duplicate();
	style_itemlist_cursor->set_draw_center(false);
	style_itemlist_cursor->set_border_width_all(border_width);
	style_itemlist_cursor->set_border_color(highlight_color);
	set_stylebox("panel", "ItemList", style_itemlist_bg);
	set_stylebox("focus", "ItemList", style_widget_focus);
	set_stylebox("cursor", "ItemList", style_itemlist_cursor);
	set_stylebox("cursor_unfocused", "ItemList", style_itemlist_cursor);
	set_stylebox("selected_focus", "ItemList", style_tree_focus);
	set_stylebox("selected", "ItemList", style_tree_selected);
	set_color("font_color", "ItemList", font_color);
	set_color("font_selected_color", "ItemList", mono_color);
	set_color("guide_color", "ItemList", guide_color);
	set_constant("v_separation", "ItemList", force_even_vsep * 0.5 * EDSCALE);
	set_constant("h_separation", "ItemList", 6 * EDSCALE);
	set_constant("icon_margin", "ItemList", 6 * EDSCALE);
	set_constant("line_separation", "ItemList", 3 * EDSCALE);

	// TabBar & TabContainer
	Ref<StyleBoxFlat> style_tabbar_background = make_flat_stylebox(dark_color_1, 0, 0, 0, 0, corner_radius * EDSCALE);
	style_tabbar_background->set_corner_radius(CORNER_BOTTOM_LEFT, 0);
	style_tabbar_background->set_corner_radius(CORNER_BOTTOM_RIGHT, 0);
	set_stylebox("tabbar_background", "TabContainer", style_tabbar_background);

	set_stylebox("tab_selected", "TabContainer", style_tab_selected);
	set_stylebox("tab_unselected", "TabContainer", style_tab_unselected);
	set_stylebox("tab_disabled", "TabContainer", style_tab_disabled);
	set_stylebox("tab_selected", "TabBar", style_tab_selected);
	set_stylebox("tab_unselected", "TabBar", style_tab_unselected);
	set_stylebox("tab_disabled", "TabBar", style_tab_disabled);
	set_stylebox("button_pressed", "TabBar", style_menu);
	set_stylebox("button_highlight", "TabBar", style_menu);
	set_color("font_selected_color", "TabContainer", font_color);
	set_color("font_unselected_color", "TabContainer", font_disabled_color);
	set_color("font_selected_color", "TabBar", font_color);
	set_color("font_unselected_color", "TabBar", font_disabled_color);
	set_color("drop_mark_color", "TabContainer", tab_highlight);
	set_color("drop_mark_color", "TabBar", tab_highlight);

	update_icon("menu", "TabContainer", SNAME("GuiTabMenu"), SNAME("EditorIcons"));
	update_icon("menu_highlight", "TabContainer", SNAME("GuiTabMenuHl"), SNAME("EditorIcons"));
	update_icon("close", "TabBar", SNAME("GuiClose"), SNAME("EditorIcons"));
	update_icon("increment", "TabContainer", SNAME("GuiScrollArrowRight"), SNAME("EditorIcons"));
	update_icon("decrement", "TabContainer", SNAME("GuiScrollArrowLeft"), SNAME("EditorIcons"));
	update_icon("increment", "TabBar", SNAME("GuiScrollArrowRight"), SNAME("EditorIcons"));
	update_icon("decrement", "TabBar", SNAME("GuiScrollArrowLeft"), SNAME("EditorIcons"));
	update_icon("increment_highlight", "TabBar", SNAME("GuiScrollArrowRightHl"), SNAME("EditorIcons"));
	update_icon("decrement_highlight", "TabBar", SNAME("GuiScrollArrowLeftHl"), SNAME("EditorIcons"));
	update_icon("increment_highlight", "TabContainer", SNAME("GuiScrollArrowRightHl"), SNAME("EditorIcons"));
	update_icon("decrement_highlight", "TabContainer", SNAME("GuiScrollArrowLeftHl"), SNAME("EditorIcons"));
	update_icon("drop_mark", "TabContainer", SNAME("GuiTabDropMark"), SNAME("EditorIcons"));
	update_icon("drop_mark", "TabBar", SNAME("GuiTabDropMark"), SNAME("EditorIcons"));

	set_constant("side_margin", "TabContainer", 0);
	set_constant("h_separation", "TabBar", 4 * EDSCALE);

	// Content of each tab.
	Ref<StyleBoxFlat> style_content_panel = style_default->duplicate();
	style_content_panel->set_border_color(dark_color_3);
	style_content_panel->set_border_width_all(border_width);
	style_content_panel->set_border_width(Side::SIDE_TOP, 0);
	style_content_panel->set_corner_radius(CORNER_TOP_LEFT, 0);
	style_content_panel->set_corner_radius(CORNER_TOP_RIGHT, 0);
	// Compensate for the border.
	style_content_panel->set_content_margin(Side::SIDE_LEFT, margin_size_extra * EDSCALE);
	style_content_panel->set_content_margin(Side::SIDE_TOP, (2 + margin_size_extra) * EDSCALE);
	style_content_panel->set_content_margin(Side::SIDE_RIGHT, margin_size_extra * EDSCALE);
	style_content_panel->set_content_margin(Side::SIDE_BOTTOM, margin_size_extra * EDSCALE);
	set_stylebox("panel", "TabContainer", style_content_panel);

	// Bottom panel.
	Ref<StyleBoxFlat> style_bottom_panel = style_content_panel->duplicate();
	style_bottom_panel->set_corner_radius_all(corner_radius * EDSCALE);
	set_stylebox("BottomPanel", "EditorStyles", style_bottom_panel);

	// TabContainerOdd can be used on tabs against the base color background (e.g. nested tabs).
	set_type_variation("TabContainerOdd", "TabContainer");

	Ref<StyleBoxFlat> style_tab_selected_odd = style_tab_selected->duplicate();
	style_tab_selected_odd->set_bg_color(disabled_bg_color);
	set_stylebox("tab_selected", "TabContainerOdd", style_tab_selected_odd);

	Ref<StyleBoxFlat> style_content_panel_odd = style_content_panel->duplicate();
	style_content_panel_odd->set_bg_color(disabled_bg_color);
	set_stylebox("panel", "TabContainerOdd", style_content_panel_odd);

	// This stylebox is used in 3d and 2d viewports (no borders).
	Ref<StyleBoxFlat> style_content_panel_vp = style_content_panel->duplicate();
	style_content_panel_vp->set_content_margin(Side::SIDE_LEFT, border_width * 2);
	style_content_panel_vp->set_content_margin(Side::SIDE_TOP, default_margin_size * EDSCALE);
	style_content_panel_vp->set_content_margin(Side::SIDE_RIGHT, border_width * 2);
	style_content_panel_vp->set_content_margin(Side::SIDE_BOTTOM, border_width * 2);
	set_stylebox("Content", "EditorStyles", style_content_panel_vp);

	// This stylebox is used by preview tabs in the Theme Editor.
	Ref<StyleBoxFlat> style_theme_preview_tab = style_tab_selected_odd->duplicate();
	style_theme_preview_tab->set_expand_margin(SIDE_BOTTOM, 5 * EDSCALE);
	set_stylebox("ThemeEditorPreviewFG", "EditorStyles", style_theme_preview_tab);
	Ref<StyleBoxFlat> style_theme_preview_bg_tab = style_tab_unselected->duplicate();
	style_theme_preview_bg_tab->set_expand_margin(SIDE_BOTTOM, 2 * EDSCALE);
	set_stylebox("ThemeEditorPreviewBG", "EditorStyles", style_theme_preview_bg_tab);

	// Separators
	set_stylebox("separator", "HSeparator", make_line_stylebox(separator_color, MAX(Math::round(EDSCALE), border_width)));
	set_stylebox("separator", "VSeparator", make_line_stylebox(separator_color, MAX(Math::round(EDSCALE), border_width), 0, 0, true));

	// Debugger

	Ref<StyleBoxFlat> style_panel_debugger = style_content_panel->duplicate();
	style_panel_debugger->set_border_width(SIDE_BOTTOM, 0);
	set_stylebox("DebuggerPanel", "EditorStyles", style_panel_debugger);

	Ref<StyleBoxFlat> style_panel_invisible_top = style_content_panel->duplicate();
	int stylebox_offset = get_font(SNAME("tab_selected"), SNAME("TabContainer"))->get_height(get_font_size(SNAME("tab_selected"), SNAME("TabContainer"))) + get_stylebox(SNAME("tab_selected"), SNAME("TabContainer"))->get_minimum_size().height + get_stylebox(SNAME("panel"), SNAME("TabContainer"))->get_content_margin(SIDE_TOP);
	style_panel_invisible_top->set_expand_margin(SIDE_TOP, -stylebox_offset);
	style_panel_invisible_top->set_content_margin(SIDE_TOP, 0);
	set_stylebox("BottomPanelDebuggerOverride", "EditorStyles", style_panel_invisible_top);

	// LineEdit

	Ref<StyleBoxFlat> style_line_edit = style_widget->duplicate();
	// The original style_widget style has an extra 1 pixel offset that makes LineEdits not align with Buttons,
	// so this compensates for that.
	style_line_edit->set_content_margin(SIDE_TOP, style_line_edit->get_content_margin(SIDE_TOP) - 1 * EDSCALE);
	// Add a bottom line to make LineEdits more visible, especially in sectioned inspectors
	// such as the Project Settings.
	style_line_edit->set_border_width(SIDE_BOTTOM, Math::round(2 * EDSCALE));
	style_line_edit->set_border_color(dark_color_2);
	// Don't round the bottom corner to make the line look sharper.
	style_tab_selected->set_corner_radius(CORNER_BOTTOM_LEFT, 0);
	style_tab_selected->set_corner_radius(CORNER_BOTTOM_RIGHT, 0);

	Ref<StyleBoxFlat> style_line_edit_disabled = style_line_edit->duplicate();
	style_line_edit_disabled->set_border_color(disabled_color);
	style_line_edit_disabled->set_bg_color(disabled_bg_color);

	set_stylebox("normal", "LineEdit", style_line_edit);
	set_stylebox("focus", "LineEdit", style_widget_focus);
	set_stylebox("read_only", "LineEdit", style_line_edit_disabled);
	update_icon("clear", "LineEdit", SNAME("GuiClose"), SNAME("EditorIcons"));
	set_color("font_color", "LineEdit", font_color);
	set_color("font_selected_color", "LineEdit", mono_color);
	set_color("font_uneditable_color", "LineEdit", font_readonly_color);
	set_color("font_placeholder_color", "LineEdit", font_placeholder_color);
	set_color("caret_color", "LineEdit", font_color);
	set_color("selection_color", "LineEdit", selection_color);
	set_color("clear_button_color", "LineEdit", font_color);
	set_color("clear_button_color_pressed", "LineEdit", accent_color);

	// TextEdit
	set_stylebox("normal", "TextEdit", style_line_edit);
	set_stylebox("focus", "TextEdit", style_widget_focus);
	set_stylebox("read_only", "TextEdit", style_line_edit_disabled);
	
	update_icon("tab", "TextEdit", SNAME("GuiTab"), SNAME("EditorIcons"));
	update_icon("space", "TextEdit", SNAME("GuiSpace"), SNAME("EditorIcons"));

	set_color("font_color", "TextEdit", font_color);
	set_color("font_readonly_color", "TextEdit", font_readonly_color);
	set_color("font_placeholder_color", "TextEdit", font_placeholder_color);
	set_color("caret_color", "TextEdit", font_color);
	set_color("selection_color", "TextEdit", selection_color);
	set_constant("line_spacing", "TextEdit", 4 * EDSCALE);

	update_icon("h_grabber", "SplitContainer", SNAME("GuiHsplitter"), SNAME("EditorIcons"));
	update_icon("v_grabber", "SplitContainer", SNAME("GuiVsplitter"), SNAME("EditorIcons"));
	update_icon("grabber", "VSplitContainer", SNAME("GuiVsplitter"), SNAME("EditorIcons"));
	update_icon("grabber", "HSplitContainer", SNAME("GuiHsplitter"), SNAME("EditorIcons"));

	set_constant("separation", "HSplitContainer", default_margin_size * 2 * EDSCALE);
	set_constant("separation", "VSplitContainer", default_margin_size * 2 * EDSCALE);

	set_constant("minimum_grab_thickness", "HSplitContainer", 6 * EDSCALE);
	set_constant("minimum_grab_thickness", "VSplitContainer", 6 * EDSCALE);

	// Containers
	set_constant("separation", "BoxContainer", default_margin_size * EDSCALE);
	set_constant("separation", "HBoxContainer", default_margin_size * EDSCALE);
	set_constant("separation", "VBoxContainer", default_margin_size * EDSCALE);
	set_constant("margin_left", "MarginContainer", 0);
	set_constant("margin_top", "MarginContainer", 0);
	set_constant("margin_right", "MarginContainer", 0);
	set_constant("margin_bottom", "MarginContainer", 0);
	set_constant("h_separation", "GridContainer", default_margin_size * EDSCALE);
	set_constant("v_separation", "GridContainer", default_margin_size * EDSCALE);
	set_constant("h_separation", "FlowContainer", default_margin_size * EDSCALE);
	set_constant("v_separation", "FlowContainer", default_margin_size * EDSCALE);
	set_constant("h_separation", "HFlowContainer", default_margin_size * EDSCALE);
	set_constant("v_separation", "HFlowContainer", default_margin_size * EDSCALE);
	set_constant("h_separation", "VFlowContainer", default_margin_size * EDSCALE);
	set_constant("v_separation", "VFlowContainer", default_margin_size * EDSCALE);

	// Custom theme type for MarginContainer with 4px margins.
	set_type_variation("MarginContainer4px", "MarginContainer");
	set_constant("margin_left", "MarginContainer4px", 4 * EDSCALE);
	set_constant("margin_top", "MarginContainer4px", 4 * EDSCALE);
	set_constant("margin_right", "MarginContainer4px", 4 * EDSCALE);
	set_constant("margin_bottom", "MarginContainer4px", 4 * EDSCALE);

	// Window

	// Prevent corner artifacts between window title and body.
	Ref<StyleBoxFlat> style_window_title = style_default->duplicate();
	style_window_title->set_corner_radius(CORNER_TOP_LEFT, 0);
	style_window_title->set_corner_radius(CORNER_TOP_RIGHT, 0);
	// Prevent visible line between window title and body.
	style_window_title->set_expand_margin(SIDE_BOTTOM, 2 * EDSCALE);

	Ref<StyleBoxFlat> style_window = style_popup->duplicate();
	style_window->set_border_color(base_color);
	style_window->set_border_width(SIDE_TOP, 24 * EDSCALE);
	style_window->set_expand_margin(SIDE_TOP, 24 * EDSCALE);
	set_stylebox("embedded_border", "Window", style_window);

	set_color("title_color", "Window", font_color);
	
	update_icon("close", "Window", SNAME("GuiClose"), SNAME("EditorIcons"));
	update_icon("close_pressed", "Window", SNAME("GuiClose"), SNAME("EditorIcons"));

	set_constant("close_h_offset", "Window", 22 * EDSCALE);
	set_constant("close_v_offset", "Window", 20 * EDSCALE);
	set_constant("title_height", "Window", 24 * EDSCALE);
	set_constant("resize_margin", "Window", 4 * EDSCALE);
	set_font("title_font", "Window", get_font(SNAME("title"), SNAME("EditorFonts")));
	set_font_size("title_font_size", "Window", get_font_size(SNAME("title_size"), SNAME("EditorFonts")));

	// Complex window (currently only Editor Settings and Project Settings)
	Ref<StyleBoxFlat> style_complex_window = style_window->duplicate();
	style_complex_window->set_bg_color(dark_color_2);
	style_complex_window->set_border_color(dark_color_2);
	set_stylebox("panel", "EditorSettingsDialog", style_complex_window);
	set_stylebox("panel", "ProjectSettingsEditor", style_complex_window);
	set_stylebox("panel", "EditorAbout", style_complex_window);

	// AcceptDialog
	set_stylebox("panel", "AcceptDialog", style_window_title);
	set_constant("buttons_separation", "AcceptDialog", 8 * EDSCALE);

	// HScrollBar
	Ref<Texture2D> empty_icon = memnew(ImageTexture);
	Ref<StyleBoxFlat> scroll_bar_bg = make_circle(style_widget);
	scroll_bar_bg->set_bg_color(mono_color * Color(1, 1, 1, 0.25));
	scroll_bar_bg->set_content_margin_all(3);
	Ref<StyleBoxFlat> scroll_bar_grabber = scroll_bar_bg->duplicate();
	scroll_bar_grabber->set_bg_color((mono_color * Color(1.0, 1.0, 1.0, 0.5)));
	Ref<StyleBoxFlat> scroll_bar_grabber_highlight = scroll_bar_grabber->duplicate();
	scroll_bar_grabber_highlight->set_bg_color((mono_color * Color(1.0, 1.0, 1.0, 1.0)).lightened(0.5));
	Ref<StyleBoxFlat> scroll_bar_grabber_pressed = scroll_bar_grabber->duplicate();
	scroll_bar_grabber_pressed->set_bg_color((mono_color * Color(1.0, 1.0, 1.0, 0.75)).lightened(0.25));
	set_stylebox("scroll", "HScrollBar", scroll_bar_bg);
	set_stylebox("scroll_focus", "HScrollBar", scroll_bar_bg);
	set_stylebox("grabber", "HScrollBar", scroll_bar_grabber);
	set_stylebox("grabber_highlight", "HScrollBar", scroll_bar_grabber_highlight);
	set_stylebox("grabber_pressed", "HScrollBar", scroll_bar_grabber_pressed);

	set_icon("increment", "HScrollBar", empty_icon);
	set_icon("increment_highlight", "HScrollBar", empty_icon);
	set_icon("increment_pressed", "HScrollBar", empty_icon);
	set_icon("decrement", "HScrollBar", empty_icon);
	set_icon("decrement_highlight", "HScrollBar", empty_icon);
	set_icon("decrement_pressed", "HScrollBar", empty_icon);

	// VScrollBar
	set_stylebox("scroll", "VScrollBar", scroll_bar_bg);
	set_stylebox("scroll_focus", "VScrollBar", scroll_bar_bg);
	set_stylebox("grabber", "VScrollBar", scroll_bar_grabber);
	set_stylebox("grabber_highlight", "VScrollBar", scroll_bar_grabber_highlight);
	set_stylebox("grabber_pressed", "VScrollBar", scroll_bar_grabber_pressed);

	set_icon("increment", "VScrollBar", empty_icon);
	set_icon("increment_highlight", "VScrollBar", empty_icon);
	set_icon("increment_pressed", "VScrollBar", empty_icon);
	set_icon("decrement", "VScrollBar", empty_icon);
	set_icon("decrement_highlight", "VScrollBar", empty_icon);
	set_icon("decrement_pressed", "VScrollBar", empty_icon);

	// HSlider
	update_icon("grabber_highlight", "HSlider", SNAME("GuiSliderGrabberHl"), SNAME("EditorIcons"));
	update_icon("grabber", "HSlider", SNAME("GuiSliderGrabber"), SNAME("EditorIcons"));

	set_stylebox("slider", "HSlider", make_flat_stylebox(dark_color_3, 0, default_margin_size / 2, 0, default_margin_size / 2, corner_width));
	set_stylebox("grabber_area", "HSlider", make_flat_stylebox(accent_color, 0, default_margin_size / 2, 0, default_margin_size / 2, corner_width));
	set_stylebox("grabber_area_highlight", "HSlider", make_flat_stylebox(accent_color, 0, default_margin_size / 2, 0, default_margin_size / 2));
	set_constant("grabber_offset", "HSlider", 0);

	// VSlider
	update_icon("grabber", "VSlider", SNAME("GuiSliderGrabber"), SNAME("EditorIcons"));
	update_icon("grabber_highlight", "VSlider", SNAME("GuiSliderGrabberHl"), SNAME("EditorIcons"));

	set_stylebox("slider", "VSlider", make_flat_stylebox(dark_color_3, default_margin_size / 2, 0, default_margin_size / 2, 0, corner_width));
	set_stylebox("grabber_area", "VSlider", make_flat_stylebox(accent_color, default_margin_size / 2, 0, default_margin_size / 2, 0, corner_width));
	set_stylebox("grabber_area_highlight", "VSlider", make_flat_stylebox(accent_color, default_margin_size / 2, 0, default_margin_size / 2, 0));
	set_constant("grabber_offset", "VSlider", 0);

	// RichTextLabel
	set_color("default_color", "RichTextLabel", font_color);
	set_color("font_shadow_color", "RichTextLabel", Color(0, 0, 0, 0));
	set_constant("shadow_offset_x", "RichTextLabel", 1 * EDSCALE);
	set_constant("shadow_offset_y", "RichTextLabel", 1 * EDSCALE);
	set_constant("shadow_outline_size", "RichTextLabel", 1 * EDSCALE);
	set_stylebox("focus", "RichTextLabel", make_empty_stylebox());
	set_stylebox("normal", "RichTextLabel", style_tree_bg);

	// Editor help.
	set_color("title_color", "EditorHelp", accent_color);
	set_color("headline_color", "EditorHelp", mono_color);
	set_color("text_color", "EditorHelp", font_color);
	set_color("comment_color", "EditorHelp", font_color * Color(1, 1, 1, 0.6));
	set_color("symbol_color", "EditorHelp", font_color * Color(1, 1, 1, 0.6));
	set_color("value_color", "EditorHelp", font_color * Color(1, 1, 1, 0.6));
	set_color("qualifier_color", "EditorHelp", font_color * Color(1, 1, 1, 0.8));
	set_color("type_color", "EditorHelp", accent_color.lerp(font_color, 0.5));
	set_color("selection_color", "EditorHelp", accent_color * Color(1, 1, 1, 0.4));
	set_color("link_color", "EditorHelp", accent_color.lerp(mono_color, 0.8));
	set_color("code_color", "EditorHelp", accent_color.lerp(mono_color, 0.6));
	set_color("kbd_color", "EditorHelp", accent_color.lerp(property_color, 0.6));
	set_constant("line_separation", "EditorHelp", Math::round(6 * EDSCALE));
	set_constant("table_h_separation", "EditorHelp", 16 * EDSCALE);
	set_constant("table_v_separation", "EditorHelp", 6 * EDSCALE);

	// Panel
	set_stylebox("panel", "Panel", make_flat_stylebox(dark_color_1, 6, 4, 6, 4, corner_width));
	set_stylebox("PanelForeground", "EditorStyles", style_default);

	// Label
	set_stylebox("normal", "Label", style_empty);
	set_color("font_color", "Label", font_color);
	set_color("font_shadow_color", "Label", Color(0, 0, 0, 0));
	set_constant("shadow_offset_x", "Label", 1 * EDSCALE);
	set_constant("shadow_offset_y", "Label", 1 * EDSCALE);
	set_constant("shadow_outline_size", "Label", 1 * EDSCALE);
	set_constant("line_spacing", "Label", 3 * EDSCALE);

	// LinkButton
	set_stylebox("focus", "LinkButton", style_empty);
	set_color("font_color", "LinkButton", font_color);
	set_color("font_hover_color", "LinkButton", font_hover_color);
	set_color("font_hover_pressed_color", "LinkButton", font_hover_pressed_color);
	set_color("font_focus_color", "LinkButton", font_focus_color);
	set_color("font_pressed_color", "LinkButton", accent_color);
	set_color("font_disabled_color", "LinkButton", font_disabled_color);

	// TooltipPanel + TooltipLabel
	// TooltipPanel is also used for custom tooltips, while TooltipLabel
	// is only relevant for default tooltips.
	Ref<StyleBoxFlat> style_tooltip = style_popup->duplicate();
	style_tooltip->set_shadow_size(0);
	style_tooltip->set_content_margin_all(default_margin_size * EDSCALE * 0.5);
	style_tooltip->set_bg_color(dark_color_3 * Color(0.8, 0.8, 0.8, 0.9));
	style_tooltip->set_border_width_all(0);
	set_color("font_color", "TooltipLabel", font_hover_color);
	set_color("font_shadow_color", "TooltipLabel", Color(0, 0, 0, 0));
	set_stylebox("panel", "TooltipPanel", style_tooltip);

	// PopupPanel
	set_stylebox("panel", "PopupPanel", style_popup);

	Ref<StyleBoxFlat> control_editor_popup_style = style_popup->duplicate();
	control_editor_popup_style->set_shadow_size(0);
	control_editor_popup_style->set_content_margin(SIDE_LEFT, default_margin_size * EDSCALE);
	control_editor_popup_style->set_content_margin(SIDE_TOP, default_margin_size * EDSCALE);
	control_editor_popup_style->set_content_margin(SIDE_RIGHT, default_margin_size * EDSCALE);
	control_editor_popup_style->set_content_margin(SIDE_BOTTOM, default_margin_size * EDSCALE);
	control_editor_popup_style->set_border_width_all(0);

	set_stylebox("panel", "ControlEditorPopupPanel", control_editor_popup_style);
	set_type_variation("ControlEditorPopupPanel", "PopupPanel");

	// SpinBox
	update_icon("updown", "SpinBox", SNAME("GuiSpinboxUpdown"), SNAME("EditorIcons"));
	update_icon("updown_disabled", "SpinBox", SNAME("GuiSpinboxUpdownDisabled"), SNAME("EditorIcons"));

	// ProgressBar
	Ref<StyleBoxFlat> progress_bar_bg = style_default->duplicate();
	progress_bar_bg->set_border_color(accent_color);
	progress_bar_bg->set_border_width_all(get_progress_bar_border_size());
	Ref<StyleBoxFlat> progress_bar_fill = progress_bar_bg->duplicate();
	progress_bar_fill->set_border_color(Color(0, 0, 0, 0));
	progress_bar_fill->set_border_width_all(progress_bar_fill->get_border_width_min() * 2);
	progress_bar_fill->set_bg_color(accent_color);
	set_stylebox("background", "ProgressBar", progress_bar_bg);
	set_stylebox("fill", "ProgressBar", progress_bar_fill);
	set_color("font_color", "ProgressBar", font_color);

	// GraphEdit
	set_stylebox("bg", "GraphEdit", style_tree_bg);
	if (dark_theme) {
		set_color("grid_major", "GraphEdit", Color(1.0, 1.0, 1.0, 0.15));
		set_color("grid_minor", "GraphEdit", Color(1.0, 1.0, 1.0, 0.07));
	} else {
		set_color("grid_major", "GraphEdit", Color(0.0, 0.0, 0.0, 0.15));
		set_color("grid_minor", "GraphEdit", Color(0.0, 0.0, 0.0, 0.07));
	}
	set_color("selection_fill", "GraphEdit", get_color(SNAME("box_selection_fill_color"), SNAME("Editor")));
	set_color("selection_stroke", "GraphEdit", get_color(SNAME("box_selection_stroke_color"), SNAME("Editor")));
	set_color("activity", "GraphEdit", accent_color);

	update_icon("minus", "GraphEdit", SNAME("ZoomLess"), SNAME("EditorIcons"));
	update_icon("more", "GraphEdit", SNAME("ZoomMore"), SNAME("EditorIcons"));
	update_icon("reset", "GraphEdit", SNAME("ZoomReset"), SNAME("EditorIcons"));
	update_icon("snap", "GraphEdit", SNAME("SnapGrid"), SNAME("EditorIcons"));
	update_icon("minimap", "GraphEdit", SNAME("GridMinimap"), SNAME("EditorIcons"));
	update_icon("layout", "GraphEdit", SNAME("GridLayout"), SNAME("EditorIcons"));

	// GraphEditMinimap
	Ref<StyleBoxFlat> style_minimap_bg = make_flat_stylebox(dark_color_1, 0, 0, 0, 0);
	style_minimap_bg->set_border_color(dark_color_3);
	style_minimap_bg->set_border_width_all(1);
	set_stylebox("bg", "GraphEditMinimap", style_minimap_bg);

	Ref<StyleBoxFlat> style_minimap_camera;
	Ref<StyleBoxFlat> style_minimap_node;
	if (dark_theme) {
		style_minimap_camera = make_flat_stylebox(Color(0.65, 0.65, 0.65, 0.2), 0, 0, 0, 0);
		style_minimap_camera->set_border_color(Color(0.65, 0.65, 0.65, 0.45));
		style_minimap_node = make_flat_stylebox(Color(1, 1, 1), 0, 0, 0, 0);
	} else {
		style_minimap_camera = make_flat_stylebox(Color(0.38, 0.38, 0.38, 0.2), 0, 0, 0, 0);
		style_minimap_camera->set_border_color(Color(0.38, 0.38, 0.38, 0.45));
		style_minimap_node = make_flat_stylebox(Color(0, 0, 0), 0, 0, 0, 0);
	}
	style_minimap_camera->set_border_width_all(1);
	set_stylebox("camera", "GraphEditMinimap", style_minimap_camera);
	set_stylebox("node", "GraphEditMinimap", style_minimap_node);

	Color minimap_resizer_color;
	if (dark_theme) {
		minimap_resizer_color = Color(1, 1, 1, 0.65);
	} else {
		minimap_resizer_color = Color(0, 0, 0, 0.65);
	}
	//set_icon("resizer", "GraphEditMinimap", get_icon(SNAME("GuiResizerTopLeft"), SNAME("EditorIcons")));
	set_color("resizer_color", "GraphEditMinimap", minimap_resizer_color);

	// GraphNode
	const int gn_margin_side = 2;
	const int gn_margin_bottom = 2;

	// StateMachine
	const int sm_margin_side = 10;

	Color graphnode_bg = dark_color_3;
	if (!dark_theme) {
		graphnode_bg = prop_section_color;
	}

	Ref<StyleBoxFlat> graphsb = make_flat_stylebox(graphnode_bg.lerp(style_tree_bg->get_bg_color(), 0.3), gn_margin_side, 24, gn_margin_side, gn_margin_bottom, corner_width);
	graphsb->set_border_width_all(border_width);
	graphsb->set_border_color(graphnode_bg);
	Ref<StyleBoxFlat> graphsbselected = make_flat_stylebox(graphnode_bg * Color(1, 1, 1, 1), gn_margin_side, 24, gn_margin_side, gn_margin_bottom, corner_width);
	graphsbselected->set_border_width_all(2 * EDSCALE + border_width);
	graphsbselected->set_border_color(Color(accent_color.r, accent_color.g, accent_color.b, 0.6));
	Ref<StyleBoxFlat> graphsbcomment = make_flat_stylebox(graphnode_bg * Color(1, 1, 1, 0.3), gn_margin_side, 24, gn_margin_side, gn_margin_bottom, corner_width);
	graphsbcomment->set_border_width_all(border_width);
	graphsbcomment->set_border_color(graphnode_bg);
	Ref<StyleBoxFlat> graphsbcommentselected = make_flat_stylebox(graphnode_bg * Color(1, 1, 1, 0.4), gn_margin_side, 24, gn_margin_side, gn_margin_bottom, corner_width);
	graphsbcommentselected->set_border_width_all(border_width);
	graphsbcommentselected->set_border_color(graphnode_bg);
	Ref<StyleBoxFlat> graphsbbreakpoint = graphsbselected->duplicate();
	graphsbbreakpoint->set_draw_center(false);
	graphsbbreakpoint->set_border_color(warning_color);
	graphsbbreakpoint->set_shadow_color(warning_color * Color(1.0, 1.0, 1.0, 0.1));
	Ref<StyleBoxFlat> graphsbposition = graphsbselected->duplicate();
	graphsbposition->set_draw_center(false);
	graphsbposition->set_border_color(error_color);
	graphsbposition->set_shadow_color(error_color * Color(1.0, 1.0, 1.0, 0.2));
	Ref<StyleBoxEmpty> graphsbslot = make_empty_stylebox(12, 0, 12, 0);
	Ref<StyleBoxFlat> smgraphsb = make_flat_stylebox(dark_color_3 * Color(1, 1, 1, 0.7), sm_margin_side, 24, sm_margin_side, gn_margin_bottom, corner_width);
	smgraphsb->set_border_width_all(border_width);
	smgraphsb->set_border_color(graphnode_bg);
	Ref<StyleBoxFlat> smgraphsbselected = make_flat_stylebox(graphnode_bg * Color(1, 1, 1, 0.9), sm_margin_side, 24, sm_margin_side, gn_margin_bottom, corner_width);
	smgraphsbselected->set_border_width_all(2 * EDSCALE + border_width);
	smgraphsbselected->set_border_color(Color(accent_color.r, accent_color.g, accent_color.b, 0.9));
	smgraphsbselected->set_shadow_size(8 * EDSCALE);
	smgraphsbselected->set_shadow_color(shadow_color);

	graphsb->set_border_width(SIDE_TOP, 24 * EDSCALE);
	graphsbselected->set_border_width(SIDE_TOP, 24 * EDSCALE);
	graphsbcomment->set_border_width(SIDE_TOP, 24 * EDSCALE);
	graphsbcommentselected->set_border_width(SIDE_TOP, 24 * EDSCALE);

	graphsb->set_corner_detail(corner_radius * EDSCALE);
	graphsbselected->set_corner_detail(corner_radius * EDSCALE);
	graphsbcomment->set_corner_detail(corner_radius * EDSCALE);
	graphsbcommentselected->set_corner_detail(corner_radius * EDSCALE);

	set_stylebox("frame", "GraphNode", graphsb);
	set_stylebox("selected_frame", "GraphNode", graphsbselected);
	set_stylebox("comment", "GraphNode", graphsbcomment);
	set_stylebox("comment_focus", "GraphNode", graphsbcommentselected);
	set_stylebox("breakpoint", "GraphNode", graphsbbreakpoint);
	set_stylebox("position", "GraphNode", graphsbposition);
	set_stylebox("slot", "GraphNode", graphsbslot);
	set_stylebox("state_machine_frame", "GraphNode", smgraphsb);
	set_stylebox("state_machine_selected_frame", "GraphNode", smgraphsbselected);

	Color node_decoration_color = dark_color_1.inverted();
	set_color("title_color", "GraphNode", node_decoration_color);
	node_decoration_color.a = 0.7;
	set_color("close_color", "GraphNode", node_decoration_color);
	set_color("resizer_color", "GraphNode", node_decoration_color);

	set_constant("port_offset", "GraphNode", 0);
	set_constant("title_h_offset", "GraphNode", 12 * EDSCALE);
	set_constant("title_offset", "GraphNode", 21 * EDSCALE);
	set_constant("close_h_offset", "GraphNode", -2 * EDSCALE);
	set_constant("close_offset", "GraphNode", 20 * EDSCALE);
	set_constant("separation", "GraphNode", 1 * EDSCALE);

	set_font("title_font", "GraphNode", get_font(SNAME("main_bold_msdf"), SNAME("EditorFonts")));

	// GridContainer
	set_constant("v_separation", "GridContainer", Math::round(widget_default_margin.y - 2 * EDSCALE));

	// FileDialog
	// Use a different color for folder icons to make them easier to distinguish from files.
	// On a light theme, the icon will be dark, so we need to lighten it before blending it with the accent color.
	set_color("folder_icon_color", "FileDialog", (dark_theme ? Color(1, 1, 1) : Color(4.25, 4.25, 4.25)).lerp(accent_color, 0.7));
	set_color("files_disabled", "FileDialog", font_disabled_color);

	// ColorPicker
	set_constant("margin", "ColorPicker", popup_margin_size);
	set_constant("sv_width", "ColorPicker", 256 * EDSCALE);
	set_constant("sv_height", "ColorPicker", 256 * EDSCALE);
	set_constant("h_width", "ColorPicker", 30 * EDSCALE);
	set_constant("label_width", "ColorPicker", 10 * EDSCALE);

	// ColorPresetButton
	Ref<StyleBoxFlat> preset_sb = make_flat_stylebox(Color(1, 1, 1), 2, 2, 2, 2, 2);
	set_stylebox("preset_fg", "ColorPresetButton", preset_sb);
	// Information on 3D viewport
	Ref<StyleBoxFlat> style_info_3d_viewport = style_default->duplicate();
	style_info_3d_viewport->set_bg_color(style_info_3d_viewport->get_bg_color() * Color(1, 1, 1, 0.5));
	style_info_3d_viewport->set_border_width_all(0);
	set_stylebox("Information3dViewport", "EditorStyles", style_info_3d_viewport);

	// Asset Library.
	set_stylebox("bg", "AssetLib", style_empty);
	set_stylebox("panel", "AssetLib", style_content_panel);
	set_color("status_color", "AssetLib", Color(0.5, 0.5, 0.5));
	// Theme editor.
	set_color("preview_picker_overlay_color", "ThemeEditor", Color(0.1, 0.1, 0.1, 0.25));
	Color theme_preview_picker_bg_color = accent_color;
	theme_preview_picker_bg_color.a = 0.2;
	Ref<StyleBoxFlat> theme_preview_picker_sb = make_flat_stylebox(theme_preview_picker_bg_color, 0, 0, 0, 0);
	theme_preview_picker_sb->set_border_color(accent_color);
	theme_preview_picker_sb->set_border_width_all(1.0 * EDSCALE);
	set_stylebox("preview_picker_overlay", "ThemeEditor", theme_preview_picker_sb);
	Color theme_preview_picker_label_bg_color = accent_color;
	theme_preview_picker_label_bg_color.set_v(0.5);
	Ref<StyleBoxFlat> theme_preview_picker_label_sb = make_flat_stylebox(theme_preview_picker_label_bg_color, 4.0, 1.0, 4.0, 3.0);
	set_stylebox("preview_picker_label", "ThemeEditor", theme_preview_picker_label_sb);

	// Dictionary editor add item.
	// Expand to the left and right by 4px to compensate for the dictionary editor margins.
	Ref<StyleBoxFlat> style_dictionary_add_item = make_flat_stylebox(prop_subsection_color, 0, 4, 0, 4, corner_radius);
	style_dictionary_add_item->set_expand_margin(SIDE_LEFT, 4 * EDSCALE);
	style_dictionary_add_item->set_expand_margin(SIDE_RIGHT, 4 * EDSCALE);
	set_stylebox("DictionaryAddItem", "EditorStyles", style_dictionary_add_item);

	Ref<StyleBoxEmpty> vshader_label_style = make_empty_stylebox(2, 1, 2, 1);
	set_stylebox("label_style", "VShaderEditor", vshader_label_style);

	// adaptive script theme constants
	// for comments and elements with lower relevance
	const Color dim_color = Color(font_color.r, font_color.g, font_color.b, 0.5);

	const float mono_value = mono_color.r;
	const Color alpha1 = Color(mono_value, mono_value, mono_value, 0.07);
	const Color alpha2 = Color(mono_value, mono_value, mono_value, 0.14);
	const Color alpha3 = Color(mono_value, mono_value, mono_value, 0.27);

	const Color symbol_color = dark_theme ? Color(0.67, 0.79, 1) : Color(0, 0, 0.61);
	const Color keyword_color = dark_theme ? Color(1.0, 0.44, 0.52) : Color(0.9, 0.135, 0.51);
	const Color control_flow_keyword_color = dark_theme ? Color(1.0, 0.55, 0.8) : Color(0.743, 0.12, 0.8);
	const Color base_type_color = dark_theme ? Color(0.26, 1.0, 0.76) : Color(0, 0.6, 0.2);
	const Color engine_type_color = dark_theme ? Color(0.56, 1, 0.86) : Color(0.11, 0.55, 0.4);
	const Color user_type_color = dark_theme ? Color(0.78, 1, 0.93) : Color(0.18, 0.45, 0.4);
	const Color comment_color = dark_theme ? dim_color : Color(0.08, 0.08, 0.08, 0.5);
	const Color string_color = dark_theme ? Color(1, 0.93, 0.63) : Color(0.6, 0.42, 0);

	// Use the brightest background color on a light theme (which generally uses a negative contrast rate).
	const Color te_background_color = dark_theme ? background_color : dark_color_3;
	const Color completion_background_color = dark_theme ? base_color : background_color;
	const Color completion_selected_color = alpha1;
	const Color completion_existing_color = alpha2;
	// Same opacity as the scroll grabber editor icon.
	const Color completion_scroll_color = Color(mono_value, mono_value, mono_value, 0.29);
	const Color completion_scroll_hovered_color = Color(mono_value, mono_value, mono_value, 0.4);
	const Color completion_font_color = font_color;
	const Color text_color = font_color;
	const Color line_number_color = dim_color;
	const Color safe_line_number_color = dark_theme ? (dim_color * Color(1, 1.2, 1, 1.5)) : Color(0, 0.4, 0, 0.75);
	const Color caret_color = mono_color;
	const Color caret_background_color = mono_color.inverted();
	const Color text_selected_color = Color(0, 0, 0, 0);
	const Color brace_mismatch_color = dark_theme ? error_color : Color(1, 0.08, 0, 1);
	const Color current_line_color = alpha1;
	const Color line_length_guideline_color = dark_theme ? base_color : background_color;
	const Color word_highlighted_color = alpha1;
	const Color number_color = dark_theme ? Color(0.63, 1, 0.88) : Color(0, 0.55, 0.28, 1);
	const Color function_color = dark_theme ? Color(0.34, 0.7, 1.0) : Color(0, 0.225, 0.9, 1);
	const Color member_variable_color = dark_theme ? Color(0.34, 0.7, 1.0).lerp(mono_color, 0.6) : Color(0, 0.4, 0.68, 1);
	const Color mark_color = Color(error_color.r, error_color.g, error_color.b, 0.3);
	const Color bookmark_color = Color(0.08, 0.49, 0.98);
	const Color breakpoint_color = dark_theme ? error_color : Color(1, 0.27, 0.2, 1);
	const Color executing_line_color = Color(0.98, 0.89, 0.27);
	const Color code_folding_color = alpha3;
	const Color search_result_color = alpha1;
	const Color search_result_border_color = dark_theme ? Color(0.41, 0.61, 0.91, 0.38) : Color(0, 0.4, 1, 0.38);

	// Now theme is loaded, apply it to CodeEdit.
	set_font("font", "CodeEdit", get_font(SNAME("source"), SNAME("EditorFonts")));
	set_font_size("font_size", "CodeEdit", get_font_size(SNAME("source_size"), SNAME("EditorFonts")));

	Ref<StyleBoxFlat> code_edit_stylebox = make_flat_stylebox(dark_color_1, widget_default_margin.x, widget_default_margin.y, widget_default_margin.x, widget_default_margin.y, corner_radius);
	set_stylebox("normal", "CodeEdit", code_edit_stylebox);
	set_stylebox("read_only", "CodeEdit", code_edit_stylebox);
	set_stylebox("focus", "CodeEdit", Ref<StyleBoxEmpty>(memnew(StyleBoxEmpty)));

	update_icon("tab", "CodeEdit", SNAME("GuiTab"), SNAME("EditorIcons"));
	update_icon("space", "CodeEdit", SNAME("GuiSpace"), SNAME("EditorIcons"));
	update_icon("folded", "CodeEdit", SNAME("CodeFoldedRightArrow"), SNAME("EditorIcons"));
	update_icon("can_fold", "CodeEdit", SNAME("CodeFoldDownArrow"), SNAME("EditorIcons"));
	update_icon("executing_line", "CodeEdit", SNAME("TextEditorPlay"), SNAME("EditorIcons"));
	update_icon("breakpoint", "CodeEdit", SNAME("Breakpoint"), SNAME("EditorIcons"));

	//set_constant("line_spacing", "CodeEdit", EDITOR_GET("text_editor/appearance/whitespace/line_spacing"));

	set_color("background_color", "CodeEdit", Color(0, 0, 0, 0));
	/*set_color("completion_background_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/completion_background_color"));
	set_color("completion_selected_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/completion_selected_color"));
	set_color("completion_existing_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/completion_existing_color"));
	set_color("completion_scroll_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/completion_scroll_color"));
	set_color("completion_scroll_hovered_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/completion_scroll_hovered_color"));
	set_color("completion_font_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/completion_font_color"));
	set_color("font_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/text_color"));
	set_color("line_number_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/line_number_color"));
	set_color("caret_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/caret_color"));
	set_color("font_selected_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/text_selected_color"));
	set_color("selection_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/selection_color"));
	set_color("brace_mismatch_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/brace_mismatch_color"));
	set_color("current_line_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/current_line_color"));
	set_color("line_length_guideline_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/line_length_guideline_color"));
	set_color("word_highlighted_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/word_highlighted_color"));
	set_color("bookmark_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/bookmark_color"));
	set_color("breakpoint_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/breakpoint_color"));
	set_color("executing_line_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/executing_line_color"));
	set_color("code_folding_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/code_folding_color"));
	set_color("search_result_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/search_result_color"));
	set_color("search_result_border_color", "CodeEdit", EDITOR_GET("text_editor/theme/highlighting/search_result_border_color"));
*/

	// Custom window
	Ref<StyleBoxFlat> custom_window = style_default->duplicate();
	custom_window->set_shadow_size(0);
	custom_window->set_content_margin_all(8);
	set_type_variation("CustomWindow", "Panel");
	set_stylebox("panel", "CustomWindow", custom_window);

	Ref<StyleBoxFlat> window_shadow = custom_window->duplicate();
	window_shadow->set_shadow_size(8);
	window_shadow->set_draw_center(false);
	set_type_variation("WindowShadow", "Panel");
	set_stylebox("panel", "WindowShadow", window_shadow);

	// Titlebar
	Ref<StyleBoxFlat> titlebar = style_default->duplicate();
	titlebar->set_bg_color(dark_color_2);
	titlebar->set_content_margin_all(0);
	titlebar->set_border_color(Color(0, 0, 0));
	titlebar->set_border_width_all(0);
	titlebar->set_border_width(Side::SIDE_BOTTOM, EDSCALE);
	titlebar->set_corner_radius_all(0);
	set_type_variation("Titlebar", "PanelContainer");
	set_stylebox("panel", "Titlebar", titlebar);

	// Window buttons
	Ref<StyleBoxFlat> icon_btn_normal = make_circle(style_widget);
	Ref<StyleBoxFlat> icon_btn_hover = make_circle(style_widget_hover);
	Ref<StyleBoxFlat> icon_btn_pressed = make_circle(style_widget_pressed);
	Ref<StyleBoxFlat> icon_btn_disabled = make_circle(style_widget_disabled);

	set_stylebox("normal", "IconButton", icon_btn_normal);
	set_stylebox("hover", "IconButton", icon_btn_hover);
	set_stylebox("pressed", "IconButton", icon_btn_pressed);
	set_stylebox("disabled", "IconButton", icon_btn_disabled);

	// Status bar
	Ref<StyleBoxFlat> status_bar = style_default->duplicate();
	status_bar->set_bg_color(mono_color * Color(1, 1, 1, 0.2));
	status_bar->set_corner_radius_all(0);
	set_type_variation("StatusBar", "PanelContainer");
	set_stylebox("panel", "StatusBar", status_bar);
	if (overrides.is_valid()) {
		merge_with(overrides);
	}
}

CustomTheme::CustomTheme() {
	accent_color = Color::from_hsv(280.0 / 360.0, 1, 1);
	base_color = accent_color.darkened(0.8);
	base_color.set_s(0.30);
	contrast = 0.28;
	border_size = 0;
	corner_radius = 8;
	progress_bar_border_size = 2;
}


void CustomTheme::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_accent_color", "accent"), &CustomTheme::set_accent_color);
	ClassDB::bind_method(D_METHOD("get_accent_color"), &CustomTheme::get_accent_color);
	ClassDB::bind_method(D_METHOD("set_base_color", "base"), &CustomTheme::set_base_color);
	ClassDB::bind_method(D_METHOD("get_base_color"), &CustomTheme::get_base_color);
	ClassDB::bind_method(D_METHOD("set_contrast", "contrast"), &CustomTheme::set_contrast);
	ClassDB::bind_method(D_METHOD("get_contrast"), &CustomTheme::get_contrast);
	ClassDB::bind_method(D_METHOD("set_border_size", "border_size"), &CustomTheme::set_border_size);
	ClassDB::bind_method(D_METHOD("get_border_size"), &CustomTheme::get_border_size);
	ClassDB::bind_method(D_METHOD("set_progress_bar_border_size", "border_size"), &CustomTheme::set_progress_bar_border_size);
	ClassDB::bind_method(D_METHOD("get_progress_bar_border_size"), &CustomTheme::get_progress_bar_border_size);
	ClassDB::bind_method(D_METHOD("set_corner_radius", "corner_radius"), &CustomTheme::set_corner_radius);
	ClassDB::bind_method(D_METHOD("get_corner_radius"), &CustomTheme::get_corner_radius);
	ClassDB::bind_method(D_METHOD("set_relationship_line_opacity", "opacity"), &CustomTheme::set_relationship_line_opacity);
	ClassDB::bind_method(D_METHOD("get_relationship_line_opacity"), &CustomTheme::get_relationship_line_opacity);
	ClassDB::bind_method(D_METHOD("is_dark_theme"), &CustomTheme::is_dark_theme);
	ClassDB::bind_method(D_METHOD("convert_dark_theme", "dark"), &CustomTheme::convert_dark_theme);
	ClassDB::bind_method(D_METHOD("update_icon", "name", "type", "icon"), &CustomTheme::update_icon);
	ClassDB::bind_method(D_METHOD("update"), &CustomTheme::update);
	ClassDB::bind_method(D_METHOD("set_base", "base"), &CustomTheme::set_base);
	ClassDB::bind_method(D_METHOD("get_base"), &CustomTheme::get_base);
	ClassDB::bind_method(D_METHOD("set_scale", "scale"), &CustomTheme::set_scale);
	ClassDB::bind_method(D_METHOD("get_scale"), &CustomTheme::get_scale);
	ClassDB::bind_method(D_METHOD("set_overrides", "overrides"), &CustomTheme::set_overrides);
	ClassDB::bind_method(D_METHOD("get_overrides"), &CustomTheme::get_overrides);

	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "accent_color"), "set_accent_color", "get_accent_color");
	ADD_PROPERTY(PropertyInfo(Variant::COLOR, "base_color"), "set_base_color", "get_base_color");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "contrast"), "set_contrast", "get_contrast");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "border_size"), "set_border_size", "get_border_size");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "scale"), "set_scale", "get_scale");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "progress_bar_border_size"), "set_progress_bar_border_size", "get_progress_bar_border_size");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "corner_radius"), "set_corner_radius", "get_corner_radius");
	ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "relationship_line_opacity"), "set_relationship_line_opacity", "get_relationship_line_opacity");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "base", PROPERTY_HINT_RESOURCE_TYPE, "Theme"), "set_base", "get_base");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "overrides", PROPERTY_HINT_RESOURCE_TYPE, "Theme"), "set_overrides", "get_overrides");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "dark_theme", PROPERTY_HINT_NONE, "", PROPERTY_USAGE_EDITOR), "convert_dark_theme", "is_dark_theme");
}

void CustomTheme::set_scale(float scale) {
	this->scale = scale;
}

float CustomTheme::get_scale() {
	return scale;
}

void CustomTheme::set_accent_color(Color accent) {
	this->accent_color = accent;
}

Color CustomTheme::get_accent_color() const {
	return accent_color;
}

void CustomTheme::set_base_color(Color base) {
	this->base_color = base;
}

Color CustomTheme::get_base_color() const {
	return base_color;
}

void CustomTheme::set_contrast(float contrast) {
	this->contrast = contrast;
}

float CustomTheme::get_contrast() const {
	return contrast;
}

void CustomTheme::set_spacing(float spacing) {
	this->spacing = spacing;
}

float CustomTheme::get_spacing() const {
	return spacing;
}

void CustomTheme::set_relationship_line_opacity(float opacity) {
	this->relationship_line_opacity = opacity;
}

float CustomTheme::get_relationship_line_opacity() const {
	return relationship_line_opacity;
}

void CustomTheme::set_border_size(int border_size) {
	this->border_size = border_size;
}

int CustomTheme::get_border_size() const {
	return border_size;
}

void CustomTheme::set_corner_radius(int corner_radius) {
	this->corner_radius = corner_radius;
}

int CustomTheme::get_corner_radius() const {
	return corner_radius;
}

void CustomTheme::set_progress_bar_border_size(int border_size) {
	this->progress_bar_border_size = border_size;
}

int CustomTheme::get_progress_bar_border_size() const {
	return progress_bar_border_size;
}

void CustomTheme::set_base(Ref<Theme> base) {
	if (base == this) {
		return;
	}
	this->base = base;
}

Ref<Theme> CustomTheme::get_base() const {
	return base;
}

void CustomTheme::set_overrides(Ref<Theme> overrides) {
	if (overrides == this) {
		return;
	}
	this->overrides = overrides;
}

Ref<Theme> CustomTheme::get_overrides() const {
	return overrides;
}
