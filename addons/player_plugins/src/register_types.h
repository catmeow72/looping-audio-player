#ifndef THEMES_REGISTER_TYPES_H
#define THEMES_REGISTER_TYPES_H

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void initialize_themes_module(ModuleInitializationLevel p_level);
void uninitialize_themes_module(ModuleInitializationLevel p_level);

#endif // THEMES_REGISTER_TYPES_H
