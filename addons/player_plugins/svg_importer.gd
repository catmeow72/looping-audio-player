@tool
extends EditorImportPlugin

func _get_importer_name():
	return "catmeow72.svg_texture"

func _get_visible_name():
	return "SVG Texture"

func _get_preset_count():
	return 1

func _get_preset_name(index):
	return "Default"

func _get_recognized_extensions():
	return ["svg"]

func _get_import_options(path, preset):
	return [
		{
			"name": "scale",
			"default_value": 1.0
		},
		{
			"name": "classes",
			"default_value": PackedStringArray(),
		}
	]

func _get_save_extension():
	return "svgtex"

func _get_resource_type():
	return "SVGTexture"

func _get_option_visibility(path, option_name, options):
	return true

func _get_priority():
	return 1000

func _get_import_order():
	return 0

func _import(source_file: String, save_path: String, options: Dictionary, platform_variants, gen_files):
	var file = FileAccess.open(save_path + "." + _get_save_extension(), FileAccess.WRITE)
	if file == null:
		return FileAccess.get_open_error()
	file.store_pascal_string(JSON.stringify(options))
	file.store_pascal_string(FileAccess.get_file_as_string(source_file))
	file.flush()
	file = null
	return OK
