extends EditorInspectorPlugin

func _can_handle(object):
	return object.is_class("CustomTheme")

func _parse_begin(object):
	if object.is_class("CustomTheme"):
		var btn = Button.new()
		btn.pressed.connect(func():
			object.update()
			ResourceSaver.save(object)
		)
		btn.text = "Update and Save"
		add_custom_control(btn)
