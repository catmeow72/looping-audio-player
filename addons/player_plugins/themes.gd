@tool
extends EditorPlugin
var plugin = load("res://addons/player_plugins/custom_theme_plugin.gd").new()
var import_plugin = load("res://addons/player_plugins/svg_importer.gd").new()

func _enter_tree():
	add_import_plugin(import_plugin)
	add_inspector_plugin(plugin)

func _exit_tree():
	remove_inspector_plugin(plugin)
	remove_import_plugin(import_plugin);
