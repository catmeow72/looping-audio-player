@tool
extends EditorPlugin

var file_dialog: EditorFileDialog

func _convert(path: String) -> bool:
	if path.ends_with(".svg"):
		var out_path = path.trim_suffix(".svg") + ".png"
		var img: Image
		var tex: Texture2D
		var res: Resource = load(path)
		if res is Image:
			img = res as Image
		elif res is Texture2D:
			tex = res as Texture2D
			img = tex.get_image()
		else:
			return false
		img.save_png(out_path)
		get_editor_interface().get_resource_filesystem().scan()
		return true
	else:
		return false

func _enter_tree():
	var editor_interface = get_editor_interface()
	var base_control = editor_interface.get_base_control()
	file_dialog = EditorFileDialog.new()
	file_dialog.access = EditorFileDialog.ACCESS_RESOURCES
	file_dialog.add_filter("*.svg")
	file_dialog.file_mode = EditorFileDialog.FILE_MODE_OPEN_FILE
	file_dialog.file_selected.connect(_convert)
	base_control.add_child(file_dialog)
	add_tool_menu_item("Convert SVG to PNG...", file_dialog.show)

func _exit_tree():
	var editor_interface = get_editor_interface()
	var base_control = editor_interface.get_base_control()
	base_control.remove_child(file_dialog)
