#GDScriptAudioImport v0.1

#MIT License
#
#Copyright (c) 2020 Gianclgar (Giannino Clemente) gianclgar@gmail.com
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#I honestly don't care that much, Kopimi ftw, but it's my little baby and I want it to look nice :3
# Modified by me for Godot 4 and to use support WAV looping metadata
class_name AudioLoader

static func report_errors(err, filepath):
	# See: https://docs.godotengine.org/en/latest/classes/class_@globalscope.html#enum-globalscope-error
	var result_hash = {
		ERR_FILE_NOT_FOUND: "File: not found",
		ERR_FILE_BAD_DRIVE: "File: Bad drive error",
		ERR_FILE_BAD_PATH: "File: Bad path error.",
		ERR_FILE_NO_PERMISSION: "File: No permission error.",
		ERR_FILE_ALREADY_IN_USE: "File: Already in use error.",
		ERR_FILE_CANT_OPEN: "File: Can't open error.",
		ERR_FILE_CANT_WRITE: "File: Can't write error.",
		ERR_FILE_CANT_READ: "File: Can't read error.",
		ERR_FILE_UNRECOGNIZED: "File: Unrecognized error.",
		ERR_FILE_CORRUPT: "File: Corrupt error.",
		ERR_FILE_MISSING_DEPENDENCIES: "File: Missing dependencies error.",
		ERR_FILE_EOF: "File: End of file (EOF) error."
	}
	if err in result_hash:
		print("Error: ", result_hash[err], " ", filepath)
	else:
		print("Unknown error with file ", filepath, " error code: ", err)

static func loadfile(filepath):
	var file = FileAccess.open(filepath, FileAccess.READ)
	if file == null:
		report_errors(FileAccess.get_open_error(), filepath)
		file.close()
		return AudioStreamWAV.new()

	# if File is wav
	if filepath.ends_with(".wav"):
		var newstream = AudioStreamWAV.new()

		#---------------------------
		#parrrrseeeeee!!! :D
		
		var bits_per_sample = 0
		
		while !file.eof_reached():
			var those4bytesbuf = file.get_buffer(4)
			if those4bytesbuf.size() == 0:
				break
			var those4bytes = those4bytesbuf.get_string_from_utf8()
			var chunksize = 0
			if those4bytes == "RIFF": 
				print_verbose("RIFF OK")
				chunksize = 4
				#RIP bytes 4-7 integer for now
			elif those4bytes == "WAVE": 
				print_verbose("WAVE OK")
				chunksize = 0
			else:
				chunksize = file.get_32()
			var filepos = file.get_position()

			if those4bytes == "fmt ":
				print_verbose("fmt OK")

				#get format code [Bytes 0-1]
				var format_code = file.get_16()
				var format_name
				if format_code == 0: format_name = "8_BITS"
				elif format_code == 1: format_name = "16_BITS"
				elif format_code == 2: format_name = "IMA_ADPCM"
				else: 
					format_name = "UNKNOWN (trying to interpret as 16_BITS)"
					format_code = 1
				print_verbose("Format: " + str(format_code) + " " + format_name)
				#assign format to our AudioStreamSample
				newstream.format = format_code
				
				#get channel num [Bytes 2-3]
				var channel_num = file.get_16()
				print_verbose("Number of channels: " + str(channel_num))
				#set our AudioStreamSample to stereo if needed
				if channel_num == 2: newstream.stereo = true
				
				#get sample rate [Bytes 4-7]
				var sample_rate = file.get_32()
				print_verbose("Sample rate: " + str(sample_rate))
				#set our AudioStreamSample mixrate
				newstream.mix_rate = sample_rate
				
				#get byte_rate [Bytes 8-11] because we can
				var byte_rate = file.get_32()
				print_verbose("Byte rate: " + str(byte_rate))
				
				#same with bits*sample*channel [Bytes 12-13]
				var bits_sample_channel = file.get_16()
				print_verbose("BitsPerSample * Channel / 8: " + str(bits_sample_channel))
				
				#aaaand bits per sample/bitrate [Bytes 14-15]
				bits_per_sample = file.get_16()
				print_verbose("Bits per sample: " + str(bits_per_sample))
				
			if those4bytes == "data":
				assert(bits_per_sample != 0)
				
				var audio_data_size = chunksize
				print_verbose("Audio data/stream size is " + str(audio_data_size) + " bytes")

				var data = file.get_buffer(chunksize)
				
				if bits_per_sample in [24, 32]:
					newstream.data = convert_to_16bit(data, bits_per_sample)
				else:
					newstream.data = data
			if those4bytes == "smpl":
				# 10 32-bit numbers are not needed
				# 32 bits = 4 bytes
				file.seek(file.get_position() + 40)
				var loop_type = file.get_32()
				var loop_type_str = "None"
				match loop_type:
					0:
						newstream.loop_mode  = AudioStreamWAV.LOOP_FORWARD
						loop_type_str = "Forward"
					1:
						newstream.loop_mode  = AudioStreamWAV.LOOP_PINGPONG
						loop_type_str = "Pingpong"
					2:
						newstream.loop_mode  = AudioStreamWAV.LOOP_BACKWARD
						loop_type_str = "Backward"
				print_verbose("Loop mode: %s" % loop_type_str)
				newstream.loop_begin = file.get_32()
				newstream.loop_end = file.get_32()
				print_verbose("Loop: %d-%d" % [newstream.loop_begin, newstream.loop_end])
			file.seek(filepos + chunksize)
			# end of parsing
			#---------------------------
		file = null
		return newstream  #:D

	#if file is mp3
	elif filepath.ends_with(".mp3"):
		var newstream = AudioStreamMP3.new()
		newstream.loop = true #set to false or delete this line if you don't want to loop
		newstream.data = file.get_buffer(file.get_length())
		return newstream
	
	elif filepath.ends_with(".ogg"):
		var newstream = OggLoader.load_ogg_vorbis(filepath)
		newstream.loop = true
		return newstream

	else:
		print("ERROR: Wrong filetype or format")
	file.close()

# Converts .wav data from 24 or 32 bits to 16
#
# These conversions are SLOW in GDScript
# on my one test song, 32 -> 16 was around 3x slower than 24 -> 16
#
# I couldn't get threads to help very much
# They made the 24bit case about 2x faster in my test file
# And the 32bit case abour 50% slower
# I don't wanna risk it always being slower on other files
# And really, the solution would be to handle it in a low-level language
static func convert_to_16bit(data: PackedByteArray, from: int) -> PackedByteArray:
	print("converting to 16-bit from %d" % from)
	var time = Time.get_ticks_msec()
	# 24 bit .wav's are typically stored as integers
	# so we just grab the 2 most significant bytes and ignore the other
	if from == 24:
		var j = 0
		for i in range(0, data.size(), 3):
			data[j] = data[i+1]
			data[j+1] = data[i+2]
			j += 2
		data.resize(data.size() * 2 / 3)
	# 32 bit .wav's are typically stored as floating point numbers
	# so we need to grab all 4 bytes and interpret them as a float first
	if from == 32:
		var spb := StreamPeerBuffer.new()
		var single_float: float
		var value: int
		for i in range(0, data.size(), 4):
			spb.data_array = data.slice(i, i+3)
			single_float = spb.get_float()
			value = single_float * 32768
			data[i/2] = value
			data[i/2+1] = value >> 8
		data.resize(data.size() / 2)
	print("Took %f seconds for slow conversion" % ((Time.get_ticks_msec() - time) / 1000.0))
	return data


# ---------- REFERENCE ---------------
# note: typical values doesn't always match

#Positions  Typical Value Description
#
#1 - 4      "RIFF"        Marks the file as a RIFF multimedia file.
#                         Characters are each 1 byte long.
#
#5 - 8      (integer)     The overall file size in bytes (32-bit integer)
#                         minus 8 bytes. Typically, you'd fill this in after
#                         file creation is complete.
#
#9 - 12     "WAVE"        RIFF file format header. For our purposes, it
#                         always equals "WAVE".
#
#13-16      "fmt "        Format sub-chunk marker. Includes trailing null.
#
#17-20      16            Length of the rest of the format sub-chunk below.
#
#21-22      1             Audio format code, a 2 byte (16 bit) integer. 
#                         1 = PCM (pulse code modulation).
#
#23-24      2             Number of channels as a 2 byte (16 bit) integer.
#                         1 = mono, 2 = stereo, etc.
#
#25-28      44100         Sample rate as a 4 byte (32 bit) integer. Common
#                         values are 44100 (CD), 48000 (DAT). Sample rate =
#                         number of samples per second, or Hertz.
#
#29-32      176400        (SampleRate * BitsPerSample * Channels) / 8
#                         This is the Byte rate.
#
#33-34      4             (BitsPerSample * Channels) / 8
#                         1 = 8 bit mono, 2 = 8 bit stereo or 16 bit mono, 4
#                         = 16 bit stereo.
#
#35-36      16            Bits per sample. 
#
#37-40      "data"        Data sub-chunk header. Marks the beginning of the
#                         raw data section.
#
#41-44      (integer)     The number of bytes of the data section below this
#                         point. Also equal to (#ofSamples * #ofChannels *
#                         BitsPerSample) / 8
#
#45+                      The raw audio data.            
