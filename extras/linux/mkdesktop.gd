func _init():
	var ifile = FileAccess.open("./LoopingAudioPlayer.desktop.in", FileAccess.READ)
	var itext = ifile.get_as_text()
	var ofile = FileAccess.open("./LoopingAudioPlayer.desktop", FileAccess.WRITE)
	ofile.store_string(itext.replace("@PATH@", OS.get_cmdline_user_args()[0]))
