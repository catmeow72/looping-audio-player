#!/bin/bash
cd "$(dirname "$0")"
rm -rf LoopingAudioPlayer.AppDir/{usr/,}lib*
for sofile in LoopingAudioPlayer.AppDir/usr/bin/*.so; do
	ldd "$sofile" |
		sed 's/\s*//g;s/.*=>//;s/(.*//' |
		grep -v 'linux-vdso.so' |
		grep -v 'libc' |
		while read -r dep; do
			mkdir -p "$(dirname LoopingAudioPlayer.AppDir/$dep)"
			cp -v "$dep" "LoopingAudioPlayer.AppDir/$dep"
		done
done
