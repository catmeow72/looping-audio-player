FROM ubuntu:latest
ENV VERSION=4.0
ENV SUBVERSION=stable
RUN apt-get -qq update
RUN apt-get -qq install -y scons build-essential librsvg2-dev libogg-dev libvorbis-dev curl unzip libfuse2 squashfs-tools git rsync file
RUN curl -Lo godot.zip https://downloads.tuxfamily.org/godotengine/$VERSION/$SUBVERSION/Godot_v$VERSION-${SUBVERSION}_linux.x86_64.zip && unzip godot.zip && rm godot.zip && mv Godot*.x86_64 /usr/local/bin/godot4
RUN curl -Lo /opt/appimagetool https://github.com/AppImage/AppImageKit/releases/download/13/appimagetool-x86_64.AppImage && chmod a+x /opt/appimagetool /usr/local/bin/godot4 && cd /tmp && dd if=/dev/zero bs=1 count=3 seek=8 conv=notrunc of=/opt/appimagetool && /opt/appimagetool --appimage-extract && rsync -r --progress squashfs-root/ / && rm -r squashfs-root /opt/appimagetool 
RUN curl -Lo /opt/export_templates.tpz https://downloads.tuxfamily.org/godotengine/$VERSION/$SUBVERSION/Godot_v$VERSION-${SUBVERSION}_export_templates.tpz
