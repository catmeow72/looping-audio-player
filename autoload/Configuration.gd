class_name ConfigNode extends Node

var config: Config = Config.new():
	get:
		return config
	set(value):
		set_process(value != null)
		config = value

var main_scene: MainScene

signal applying(cfg: Config)
signal theme_update(theme: Theme)
signal window_btn_update(layout: WinBtnLayout)
signal error(important: bool, err_str: String)
signal loaded
signal saved

const UPDATE_INTERVAL: float = 0.125
var update_timeout: float = 0.0

func wait():
	if !ready_finished:
		await ready_just_finished

signal ready_just_finished
var ready_finished: bool = false

func save():
	var file = FileAccess.open("user://config.json", FileAccess.WRITE)
	if file != null:
		file.store_string(JSON.stringify(config.serialize()))
		saved.emit()
	else:
		error.emit(true, "Opening the configuration file for saving has failed.")

func load_config():
	var file = FileAccess.open("user://config.json", FileAccess.READ)
	var new_config = Config.new()
	if file != null:
		var parsed = JSON.parse_string(file.get_as_text())
		var dict: Dictionary = {}
		if parsed is Dictionary:
			dict = parsed as Dictionary
		else:
			return
		new_config.deserialize(dict)
	config = new_config
	_update_dark_mode(true)
	loaded.emit()

func apply():
	apply_visual()
	applying.emit(config)

func apply_visual():
	_update_dark_mode(true)
var dark_theme: Theme
var light_theme: Theme
func _enter_tree():
	set_process(false)
	
func _exit_tree():
	save()


func _parse_layout_split(str: String) -> Array[WinBtnLayout.Entry]:
	var output: Array[WinBtnLayout.Entry] = []
	var entries = str.split(",", false)
	for entry in entries:
		match entry.to_lower():
			"minimize":
				output.append(WinBtnLayout.Entry.Minimize)
			"maximize":
				output.append(WinBtnLayout.Entry.Maximize)
			"close":
				output.append(WinBtnLayout.Entry.Close)
			"spacer":
				output.append(WinBtnLayout.Entry.Spacer)
	return output

var layout: WinBtnLayout
var prev_layout: String = ""

func _update_layout():
	var new_layout: WinBtnLayout = WinBtnLayout.new()
	if config.layout != null:
		new_layout = config.layout
	else:
		if OS.has_feature("linux"):
			var gnome_layout = preload("res://addons/csd/layouts/Gnome.tres")
			var output: Array[String] = []
			if OS.execute("dconf", ["read", "/org/gnome/desktop/wm/preferences/button-layout"], output, false) >= 0:
				var layout_setting: String = output[0]
				if !layout_setting.is_empty():
					layout_setting = layout_setting.lstrip('\'').rstrip('\'\n')
					var left: String = ""
					var right: String = ""
					var setting_split = (layout_setting + ":").split(":", true, 2)
					left = setting_split[0]
					right = setting_split[1]
					new_layout.left_buttons = _parse_layout_split(left)
					new_layout.right_buttons = _parse_layout_split(right)
				else:
					new_layout = gnome_layout
			else:
				new_layout = gnome_layout
		elif OS.has_feature("windows") || OS.has_feature("uwp"):
			new_layout = preload("res://addons/csd/layouts/Windows.tres")
		elif OS.has_feature("macos"):
			new_layout = preload("res://addons/csd/layouts/MacOS.tres")
		else:
			new_layout = WinBtnLayout.new()
	if layout == null || (new_layout.left_buttons.hash() != layout.left_buttons.hash() || new_layout.right_buttons.hash() != layout.right_buttons.hash()):
		layout = new_layout
		window_btn_update.emit(layout)

# Called when the node enters the scene tree for the first time.
func _ready():
	_update_layout()
	dark_theme = await CustomMainLoop.load_async("res://themes/dark.tres")
	light_theme = await CustomMainLoop.load_async("res://themes/light.tres")
	theme_update.connect(func(theme: Theme):
		get_tree().root.theme = theme
	)
	load_config()
	set_process(true)
	ready_finished = true
	ready_just_finished.emit()

var was_dark_mode: bool = false

func is_dark_mode():
	match config.dark_mode:
		DarkModeEnum.DARK_MODE:
			return true
		DarkModeEnum.LIGHT_MODE:
			return false
		DarkModeEnum.USE_SYSTEM:
			return DisplayServer.is_dark_mode()
		_:
			return true

func _update_dark_mode(force: bool = false):
	var is_dark_mode = is_dark_mode()
	if force || was_dark_mode != is_dark_mode:
		theme_update.emit((dark_theme if is_dark_mode else light_theme) as Theme)
		was_dark_mode = is_dark_mode

func _process(delta):
	update_timeout += delta
	if update_timeout > UPDATE_INTERVAL:
		update_timeout -= UPDATE_INTERVAL
		_update_dark_mode()
		_update_layout()
