extends Node

var player: AudioStreamPlayer

signal update()

var paused: bool:
	get:
		return !player.playing
	set(value):
		player.stream_paused = value
		update.emit()

var playing: bool:
	get:
		return player.playing || player.stream_paused

var speed: float:
	get:
		return player.pitch_scale
	set(value):
		player.pitch_scale = value

var volume: float:
	get:
		var volume: float = ((1.0 - (sqrt(-AudioServer.get_bus_volume_db(0)) / 7.0)) * 100.0)
		return 0.0 if AudioServer.is_bus_mute(0) else volume
	set(value):
		Configuration.config.volume = value
		var vol_db = -(((1.0 - (value / 100.0)) * 7.0) ** 2.0)
		AudioServer.set_bus_volume_db(0, vol_db)
		AudioServer.set_bus_mute(0, vol_db <= -59.9999)

var position: float:
	get:
		return player.get_playback_position()
	set(value):
		seek(value)

var playing_path: String = ""

func seek(position: float):
	if playing:
		if paused:
			paused = true
			player.play(position)
			paused = true
		else:
			paused = true
			player.play(position)
		update.emit()

func get_playback_position():
	return player.get_playback_position()

func toggle_pause():
	paused = !paused

func restart_stream():
	paused = true
	player.play(0)
	update.emit()

func stop():
	player.stop()
	playing_path = ""
	update.emit()

# Called when the node enters the scene tree for the first time.
func _ready():
	player = AudioStreamPlayer.new()
	add_child(player)
	player.finished.connect(restart_stream)

func parse_args():
	var args = OS.get_cmdline_user_args()
	for i in args:
		if FileAccess.file_exists(i):
			play(i)
			break

func play(path: String):
	var new_stream = AudioLoader.loadfile(path)
	player.stream = new_stream
	player.play()
	playing_path = path
	update.emit()

func extract_loop_info() -> Array[float]:
	var output: Array[float] = [0.0, player.stream.get_length()]
	if player.stream is AudioStreamOggVorbis:
		var stream = player.stream as AudioStreamOggVorbis
		output = [stream.loop_offset, stream.get_length() - stream.loop_offset]
	elif player.stream is AudioStreamWAV:
		var stream = player.stream as AudioStreamWAV
		var div = stream.mix_rate as float
		var begin = stream.loop_begin as float / div
		var end = stream.loop_end as float / div
		if end <= 0:
			end = stream.get_length()
		output = [begin, end]
		
	return output
