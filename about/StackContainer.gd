@tool
class_name Stack extends Container
@export
var max_pages: int:
	get:
		return get_children().filter(func(child):
			return child is Control && child.visible
		).size()
@export
var page: int = 0:
	set(value):
		var old_page = page
		page = clampi(value, 0, max_pages - 1)
		scroll = get_rect().size.x * (1 if page > old_page else -1)
		if page == 0:
			min_page_reached.emit()
		if page != max_pages - 1:
			not_max_page.emit()
		if page == max_pages - 1:
			max_page_reached.emit()
		if page != 0:
			not_min_page.emit()
signal min_page_reached()
signal max_page_reached()
signal not_min_page()
signal not_max_page()
var scroll: int = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _get_minimum_size():
	return get_children().reduce(func(output, child):
		var combined_min_size = child.get_combined_minimum_size()
		return Vector2(max(combined_min_size.x, output.x), max(combined_min_size.y, output.y))
	, Vector2.ZERO)

func _notification(what):
	match what:
		NOTIFICATION_SORT_CHILDREN:
			var scroll = page * get_rect().size.x
			var child_rect: Rect2i = Rect2i(Vector2i(self.scroll - scroll, 0), get_rect().size)
			for i in get_children():
				if i is Control && i.visible:
					fit_child_in_rect(i, child_rect)
					child_rect.position += Vector2i(child_rect.size.x, 0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	scroll *= 0.95
	queue_sort()
