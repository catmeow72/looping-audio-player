extends ThemedWindow

@onready var back_btn: Button = $Panel/Panel/VBoxContainer/VBoxContainer/HBoxContainer/BackBtn
@onready var next_btn: Button = $Panel/Panel/VBoxContainer/VBoxContainer/HBoxContainer/NextBtn
@onready var stack: Stack = $Panel/Panel/VBoxContainer/VBoxContainer/Main/Stack
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_confirm():
	hide()

func _on_back_btn_pressed():
	stack.page -= 1

func _on_stack_max_page_reached():
	next_btn.disabled = true

func _on_stack_min_page_reached():
	back_btn.disabled = true

func _on_stack_not_max_page():
	next_btn.disabled = false

func _on_stack_not_min_page():
	back_btn.disabled = false

func _on_next_btn_pressed():
	stack.page += 1
