@tool
extends Control

func _ready():
	queue_redraw()

func _notification(what):
	match what:
		NOTIFICATION_THEME_CHANGED:
			queue_redraw()

func _draw():
	draw_style_box(get_theme_stylebox("Background", "EditorStyles"), get_rect())
