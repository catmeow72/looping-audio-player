extends Tree


@onready var ctx_menu: PopupMenu = $ContextMenu
@onready var cat_edit: PopupPanel = $CategoryEdit
@onready var cat_edit_icon: TextureRect = $CategoryEdit/VBoxContainer/TitleContainer/Icon
@onready var cat_edit_label: Label = $CategoryEdit/VBoxContainer/TitleContainer/Label
@onready var cat_edit_option_btn: OptionButton = $CategoryEdit/VBoxContainer/OptionButton
@onready var cat_edit_line_edit: LineEdit = $CategoryEdit/VBoxContainer/LineEdit

var change_category_tex: SVGTexture
var rename_tex: SVGTexture
var play_tex: SVGTexture
var stop_tex: SVGTexture
var del_tex: SVGTexture
var cat_tex: SVGTexture
var mus_tex: SVGTexture
const UNCATEGORIZED = "Uncategorized"

var categories: Dictionary = {}
var files: Dictionary = {}

enum ButtonID {
	PLAY,
	CHANGE_CATEGORY,
	RENAME,
	DELETE,
}

func get_file(category: String, idx: int):
	return categories[category].get_child(idx)

func add_category(category: String) -> TreeItem:
	if category == UNCATEGORIZED:
		category = ""
	if categories.has(category):
		return categories[category]
	var item = create_item(get_root())
	item.set_icon(0, cat_tex)
	item.set_metadata(0, category)
	item.set_text(0, UNCATEGORIZED if category == "" else category)
	if category == "":
		item.add_button(0, change_category_tex, ButtonID.CHANGE_CATEGORY)
	else:
		item.add_button(0, rename_tex, ButtonID.RENAME)
		item.add_button(0, del_tex, ButtonID.DELETE)
	categories[category] = item
	return item

func add_file(name: String, path: String, category: String = ""):
	var item = create_item(add_category(category))
	item.set_icon(0, mus_tex)
	item.set_editable(0, false)
	item.set_text(0, name)
	item.set_metadata(0, path)
	item.add_button(0, play_tex, ButtonID.PLAY)
	item.add_button(0, change_category_tex, ButtonID.CHANGE_CATEGORY)
	item.add_button(0, rename_tex, ButtonID.RENAME)
	item.add_button(0, del_tex, ButtonID.DELETE)
	files[path] = item
	_save()

func _load():
	clear()
	var root = create_item()
	root.set_text(0, "Root")
	var file = FileAccess.open("user://favorites.json", FileAccess.READ)
	if file != null:
		var favorites = JSON.parse_string(file.get_as_text()) 
		if favorites == null:
			return
		for fav in favorites:
			if !(fav.path is String):
				continue
			if !FileAccess.file_exists(fav.path):
				continue
			if !("name" in fav):
				fav.name = fav.path.get_file().get_basename()
			add_file(fav.name, fav.path, fav.category if "category" in fav else "")

func _save():
	var file = FileAccess.open("user://favorites.json", FileAccess.WRITE)
	var favorites: Array[Dictionary] = []
	for category in categories.keys():
		for item in categories[category].get_children():
			favorites.append({
				"name": item.get_text(0),
				"path": item.get_metadata(0),
				"category": category
			})
	file.store_string(JSON.stringify(favorites))
	file.flush()
	file = null

func _exit_tree():
	_save()
	
func update_textures():
	var classes = PackedStringArray(["dark" if Configuration.is_dark_mode() else "light"])
	change_category_tex.classes = classes
	rename_tex.classes = classes
	play_tex.classes = classes
	stop_tex.classes = classes
	del_tex.classes = classes
	cat_tex.classes = classes
	mus_tex.classes = classes

# Called when the node enters the scene tree for the first time.
func _ready():
	var scale = 1.5
	change_category_tex = load("res://themes/change_category.svg")
	change_category_tex.scale = scale
	rename_tex = load("res://themes/rename_colorized.svg")
	rename_tex.scale = scale
	play_tex = load("res://themes/play_colorized.svg")
	play_tex.scale = scale
	stop_tex = load("res://themes/stop_colorized.svg")
	stop_tex.scale = scale
	cat_tex = load("res://themes/folder.svg")
	cat_tex.scale = scale
	mus_tex = load("res://themes/music_icon.svg")
	mus_tex.scale = scale
	del_tex = load("res://addons/csd/icons/close.svg")
	del_tex.scale = scale * (16.0 / 20.0)
	cat_edit_icon.texture = change_category_tex
	update_textures()
	theme_changed.connect(update_textures)
	set_column_title(0, "Filename")
	_load()
	Player.update.connect(func():
		var item = files[Player.playing_path] if files.has(Player.playing_path) else null
		playing_file = item
	)

func _gui_input(event):
	var vol_presets = [1, 5, 10]
	var vol_preset = 1
	var speed_presets = [0.05, 0.10, 0.25]
	var speed_preset = 1
	if event is InputEventKey:
		if event.shift_pressed:
			speed_preset -= 1
			vol_preset -= 1
		if event.ctrl_pressed:
			speed_preset += 1
			vol_preset += 1
	if event.is_action_pressed("change_category"):
		var item = get_next_selected(null)
		if item != null:
			_on_button_clicked(item, 0, ButtonID.CHANGE_CATEGORY, 0)
		accept_event()
	if event.is_action_pressed("stop"):
		Player.stop()
		accept_event()
	if event.is_action("faster") && event.is_pressed():
		Player.speed += speed_presets[speed_preset]
		accept_event()
	if event.is_action("slower") && event.is_pressed():
		Player.speed -= speed_presets[speed_preset]
		accept_event()
	if event.is_action("seek_left") && event.is_pressed():
		Player.position -= 1.0
		accept_event()
	if event.is_action("seek_right") && event.is_pressed():
		Player.position += 1.0
		accept_event()
	if event.is_action("vol_up") && event.is_pressed():
		Player.volume = min(Player.volume + vol_presets[vol_preset], 100)
		accept_event()
	if event.is_action("vol_down") && event.is_pressed():
		Player.volume = max(Player.volume - vol_presets[vol_preset], 0)
		accept_event()
	if event.is_action_pressed("pause"):
		if Player.playing:
			Player.toggle_pause()
		else:
			var selection = get_next_selected(null)
			if selection != null && files.values().has(selection):
				play.emit(selection.get_metadata(0))
		accept_event()
	if event.is_action_pressed("rename"):
		_on_button_clicked(get_next_selected(null), 0, ButtonID.RENAME, 0)
		accept_event()
	if event.is_action_pressed("delete"):
		_on_button_clicked(get_next_selected(null), 0, ButtonID.DELETE, 0)
		accept_event()
	if event is InputEventMouseButton:
		var mbevent = event as InputEventMouseButton
		if mbevent.button_index == MOUSE_BUTTON_RIGHT:
			var selection: TreeItem = get_selected()
			if selection != null:
				var selected_idx = get_item_at_position(get_local_mouse_position())
				set_selected(selected_idx, 0)
				selection = selected_idx
			if selection == null:
				return
			ctx_menu.set_item_disabled(ctx_menu.get_item_index(0), selection == null && selection.get_parent() != get_root())
			ctx_menu.set_item_disabled(ctx_menu.get_item_index(1), selection == null && selection.get_parent() != get_root())
			ctx_menu.position = get_viewport().get_mouse_position() as Vector2i + get_viewport().position
			ctx_menu.popup()
			accept_event()

signal play(path: String)

func _on_context_menu_id_pressed(id):
	var selection = get_next_selected(null)
	if selection == null:
		return
	match id:
		0:
			play.emit(selection.get_metadata(0))
		1:
			selection.free()
		2:
			pass

func _on_item_activated():
	var selection = get_next_selected(null)
	if selection != null:
		var meta = selection.get_metadata(0)
		if typeof(meta) == TYPE_STRING && FileAccess.file_exists(meta):
			play.emit(meta)
			playing_file = selection

var category_edit_item: TreeItem

func _on_cancel_btn_pressed():
	cat_edit.hide()

func change_category(item: TreeItem, category: String):
	if item == null:
		return
	var cat_item = item.get_parent()
	cat_item.remove_child(item)
	files.erase(item.get_metadata(0))
	if cat_item.get_child_count() == 0:
		categories.erase(cat_item.get_metadata(0))
		cat_item.free()
	add_file(item.get_text(0), item.get_metadata(0), category)
	if playing_file == item:
		playing_file = files[item.get_metadata(0)]
	item.free()

func _on_ok_btn_pressed():
	cat_edit.hide()
	var category = cat_edit_line_edit.text
	
	var item = category_edit_item
	if categories.values().has(item):
		for child in item.get_children():
			change_category(child, category)
	else:
		change_category(item, category)
	_save()

var playing_file: TreeItem:
	set(value):
		if playing_file == value:
			return
		if playing_file != null:
			playing_file.set_button(0, playing_file.get_button_by_id(0, ButtonID.PLAY), play_tex)
		playing_file = value
		if playing_file != null:
			playing_file.set_button(0, playing_file.get_button_by_id(0, ButtonID.PLAY), stop_tex)

func _on_button_clicked(item: TreeItem, column: int, id: int, mouse_button_index: int):
	var is_category = categories.values().has(item)
	match id:
		ButtonID.CHANGE_CATEGORY:
			category_edit_item = item
			cat_edit_option_btn.clear()
			if !categories.keys().has(""):
				cat_edit_option_btn.add_item(UNCATEGORIZED)
				cat_edit_option_btn.set_item_metadata(cat_edit_option_btn.item_count - 1, {"editable": false, "value": ""})
			for category in categories.keys():
				if !is_category || categories[category] != item:
					cat_edit_option_btn.add_item(categories[category].get_text(0))
					var idx = cat_edit_option_btn.item_count - 1
					cat_edit_option_btn.set_item_metadata(idx, {"editable": false, "value": category})
			if !is_category:
				var idx = categories.keys().find(item.get_parent().get_metadata(0))
				cat_edit_option_btn.select(idx)
				_on_option_button_item_selected(idx)
			cat_edit_option_btn.add_item("Other...")
			var idx = cat_edit_option_btn.item_count - 1
			cat_edit_option_btn.set_item_metadata(idx, {"editable": true, "value": ""})
			if is_category:
				cat_edit_option_btn.select(idx)
				_on_option_button_item_selected(idx)
			cat_edit_label.text = item.get_text(0)
			var vp_rect = get_viewport_rect()
			cat_edit_option_btn.grab_focus()
			cat_edit.popup_on_parent(vp_rect)
		ButtonID.RENAME:
			item.select(0)
			item.set_editable(0, true)
			edit_selected()
		ButtonID.PLAY:
			if is_category:
				return
			if Player.playing_path == item.get_metadata(0):
				Player.stop()
			else:
				play.emit(item.get_metadata(0))
		ButtonID.DELETE:
			if is_category:
				if item.get_metadata(0) == "":
					return
				for child in item.get_children():
					change_category(child, "")
			else:
				files.erase(item.get_metadata(0))
				item.free()

func _on_option_button_item_selected(index):
	var metadata = cat_edit_option_btn.get_item_metadata(index)
	if metadata is Dictionary:
		if metadata.has_all(["editable", "value"]):
			cat_edit_line_edit.text = metadata.value
			cat_edit_line_edit.editable = metadata.editable
			cat_edit_line_edit.visible = metadata.editable

func _on_item_edited():
	var item: TreeItem = get_edited()
	if item.get_parent() == get_root():
		var key = categories.find_key(item)
		if categories.has(item.get_text(0)):
			item.set_text(0, key)
		else:
			categories.erase(key)
			categories[item.get_text(0)] = item
	item.set_editable(0, false)
	_save()

func _on_line_edit_text_submitted(new_text):
	_on_ok_btn_pressed()
