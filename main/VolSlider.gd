extends HSlider

var dragging: bool = false
@onready var label: Label = $"../VolLabel"

func _enter_tree():
	Configuration.applying.connect(func(cfg: Config):
		value = cfg.volume
	)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _value_changed(new_value):
	Player.volume = new_value
	label.text = "%d%%" % new_value
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !dragging:
		value = Player.volume

func _on_drag_started():
	dragging = false

func _on_drag_ended(value_changed):
	dragging = true
