extends Control
class_name MainScene

@onready var file_list: Tree = $VBoxContainer/Favorites
@onready var pause_btn: IconButton = $VBoxContainer/HBoxContainer/PauseBtn
@onready var beginning_btn: IconButton = $VBoxContainer/HBoxContainer/BeginningBtn
@onready var seek_slider: Slider = $VBoxContainer/HBoxContainer/SeekSlider
@onready var stop_btn: IconButton = $VBoxContainer/HBoxContainer/StopBtn
@onready var file_dialog: FileDialog = $FileDialog
@onready var status_label: Label = $%StatusLabel
@onready var prefs_window: Node = $PrefsWindow
@onready var about_window: Node = $AboutWindow
@export_node_path("MainMenu")
var menu_bar_path: NodePath
var menu_bar: MainMenu:
	get:
		return get_node(menu_bar_path)

var title: String:
	get:
		return get_tree().root.title
	set(value):
		get_tree().root.title = value

# Called when the node enters the scene tree for the first time.
func _ready():
	_update_pause_status()
	var win = (get_viewport() as Window)
	win.wrap_controls = false
	Configuration.applying.connect(func(cfg: Config):
		file_dialog.current_dir = cfg.last_opened_dir
	)
	Configuration.theme_update.connect(func(new_theme: Theme):
		theme = new_theme
	)
	Configuration.load_config()
	Configuration.apply()
	await Configuration.wait()
	Player.update.connect(_update_pause_status)
	LoadingScr.hide()
	Player.parse_args()
	get_tree().root.disable_3d = true
	file_list.grab_focus()

var progress_bar_tween: Tween = null
const progress_tween_duration: float = 0.5
var looping: bool = false
var was_hidden_hint: bool = true
var seeking: bool = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pause_btn.disabled = !Player.playing
	beginning_btn.disabled = !Player.playing
	seek_slider.editable = Player.playing
	stop_btn.disabled = !Player.playing
	if !Player.paused && !seeking:
		var loop_info = Player.extract_loop_info()
		var min_value = loop_info[0]
		var max_value = loop_info[1]
		var value = Player.get_playback_position()
		var looping_prev = looping
		looping = value >= min_value
		if !looping:
			max_value = min_value
			min_value = 0
		
		status_label.text = "Playing loop" if looping else "Playing beginning"
			
		if !was_hidden_hint && progress_bar_tween == null:
			if looping != looping_prev || (value - min_value < 0.1):
				var tween = get_tree().create_tween().set_parallel().bind_node(seek_slider)
				var ratio = seek_slider.ratio
				tween.tween_method(func(percent: float):
					seek_slider.ratio = lerp(ratio, remap(Player.get_playback_position(), seek_slider.min_value, seek_slider.max_value, 0, 1), percent)
				, 0.0, 1.0, progress_tween_duration)
				progress_bar_tween = tween
				progress_bar_tween.finished.connect(func():
					progress_bar_tween = null
				)
				progress_bar_tween.play()
				
		seek_slider.min_value = min_value
		seek_slider.max_value = max_value
		if progress_bar_tween != null && progress_bar_tween.is_running():
			pass
		else:
			seek_slider.value = value
		if value - min_value >= 0.1:
			was_hidden_hint = false
	else:
		status_label.text = "Seeking" if seeking else "Paused" if Player.paused else "Stopped"

func _on_play_btn_pressed():
	var selected = file_list.get_selected()
	if selected != null:
		var file = selected.get_metadata(0) as String
		play(file)

func _on_pause_btn_pressed():
	Player.toggle_pause()

func _update_pause_status():
	if !Player.playing:
		title = "Audio Player"
	pause_btn.icon = preload("res://themes/play.svg") if Player.paused else preload("res://themes/pause.svg")

func _on_stop_btn_pressed():
	Player.stop()


func _on_favorites_item_selected():
	menu_bar.set_remove_favorites_disabled(false)


func _on_open_btn_pressed(now_adding: bool = false):
	self.adding = now_adding
	file_dialog.clear_filters()
	for i in [["wav", "WAV format audio"], ["mp3", "MP3 music"], ["ogg", "Ogg Vorbis music"]]:
		file_dialog.add_filter("*." + i[0], i[1])
	file_dialog.file_mode = FileDialog.FILE_MODE_OPEN_FILES if adding else FileDialog.FILE_MODE_OPEN_FILE
	file_dialog.access = FileDialog.ACCESS_FILESYSTEM
	file_dialog.popup_on_parent(get_viewport_rect())

var adding: bool = false
func play(path: String):
	Player.play(path)
	title = path.get_file().get_basename() + " - Audio Player"
	was_hidden_hint = !seek_slider.is_visible_in_tree()

func _add_file(path: String):
	file_list.add_file(path.get_file().get_basename(), path)
	
func _on_file_dialog_file_selected(path: String):
	if adding:
		_add_file(path)
	else:
		play(path)

func _on_del_btn_pressed():
	var selection = file_list.get_selected()
	if selection != null:
		selection.free()
	file_list.deselect_all()
	menu_bar.set_remove_favorites_disabled(true)


func _on_favorites_play(path):
	play(path)

func _on_file_dialog_files_selected(paths: PackedStringArray):
	for path in paths:
		_add_file(path)

var paused_before_seek: bool = false

func _on_seek_slider_drag_started():
	seeking = true
	paused_before_seek = Player.paused
	Player.paused = true

func _on_seek_slider_drag_ended(value_changed: bool):
	seeking = false
	Player.paused = false
	Player.seek(seek_slider.value)
	Player.paused = paused_before_seek

func _on_beginning_btn_pressed():
	Player.paused = false
	Player.seek(0)


func _on_main_menu_add_favorites():
	_on_open_btn_pressed(true)


func _on_main_menu_open_file():
	_on_open_btn_pressed(false)

func _on_main_menu_prefs_requested():
	prefs_window.popup_on_parent(get_viewport_rect())


func _on_main_menu_about_requested():
	about_window.popup_on_parent(get_viewport_rect())

func _exit_tree():
	Configuration.config.last_opened_dir = file_dialog.current_dir
	Configuration.save()
