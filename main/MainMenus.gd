extends MenuBar
class_name MainMenu
@onready var file: PopupMenu = $File

signal add_favorites()
signal open_file()
signal prefs_requested()
signal about_requested()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_remove_favorites_disabled(disabled: bool):
	file.set_item_disabled(file.get_item_index(201), disabled)

func _on_file_id_pressed(id):
	match id:
		100:
			open_file.emit()
		200:
			add_favorites.emit()
		900:
			get_tree().quit()

func _on_edit_id_pressed(id):
	match id:
		100:
			prefs_requested.emit()


func _on_help_id_pressed(id):
	match id:
		100:
			about_requested.emit()
