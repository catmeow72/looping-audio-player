extends HSlider

var dragging: bool = false
@onready var label: Label = $"../SpeedLabel"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _value_changed(new_value):
	if dragging && abs(new_value - 1.0) < 0.125:
		value = 1.0
	Player.speed = value
	label.text = "%.2fx" % value
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !dragging:
		value = Player.speed

func _on_drag_started():
	dragging = false

func _on_drag_ended(value_changed):
	dragging = true
