extends Serializable
class_name Config


# Export annotations are used for (de)serialization.

@export_enum("system", "dark", "light")
var dark_mode: int = DarkModeEnum.USE_SYSTEM
@export
var last_opened_dir: String = ""
@export
var volume: float = 100.0
@export
var enable_csd: bool = true
@export
var layout: WinBtnLayout = null
