class_name Serializable extends Resource

func _enum_strs(enum_str: String):
	var output: Dictionary = {}
	var enum_strs = enum_str.split(",")
	var i = 0
	for str in enum_strs:
		var name = str
		var value = i
		if name.contains(":"):
			var colon_pos = name.rfind(":")
			var value_str = name.substr(colon_pos + 1)
			if value_str.is_valid_int():
				value = value_str.to_int()
				name = name.left(colon_pos)
		output[name] = value
		output[value] = name
		i = value + 1
	return output

func serialize():
	var output: Dictionary = {}
	for property in get_property_list():
		if property.usage & PROPERTY_USAGE_SCRIPT_VARIABLE:
			var name = property.name
			var value = get(name)
			if property.type == TYPE_OBJECT:
				if value != null:
					value = inst_to_dict(value)
			if property.hint == PROPERTY_HINT_ENUM && property.type == TYPE_INT:
				value = _enum_strs(property.hint_string)[value]
			output[name] = value
	return output

func deserialize(dict: Dictionary):
	for property in get_property_list():
		if property.usage & PROPERTY_USAGE_SCRIPT_VARIABLE:
			var name = property.name
			if !dict.has(name):
				continue
			var value = dict[name]
			if property.type == TYPE_OBJECT:
				if value != null:
					value = dict_to_inst(value)
			if property.type == TYPE_INT:
				value = int(value)
			if property.hint == PROPERTY_HINT_ENUM && property.type == TYPE_INT:
				value = _enum_strs(property.hint_string)[value]
			set(name, value)
