extends HBoxContainer

@onready var layout: WinBtnLayout = null

func _on_visibility_changed():
	layout = Configuration.layout

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _can_drop_data(at_position, data):
	return data is Dictionary && "entry" in data && "preview" in data

func _drop_data(at_position, data):
	for child in get_children(true):
		if child is Control:
			child = child as Control
			var rect = child.get_rect()
			if rect.has_point(at_position):
				if child.get_meta("left", true):
					layout.left_buttons.append(data.entry)
					get_node("LeftSide").add_child(data.preview)
				else:
					layout.right_buttons.append(data.entry)
					get_node("RightSide").add_child(data.preview)
