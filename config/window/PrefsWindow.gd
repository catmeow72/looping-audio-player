extends ThemedWindow

@onready var dark_mode_opts: OptionButton = $%Options/DarkModeOpts
@onready var csd_check_btn: CheckButton = $%Options/CsdCheckBtn
@onready var revert_btn: Control = $%Buttons/RevertBtn
@onready var save_btn: Control = $%Buttons/SaveBtn
func disable_revert_save(disabled: bool):
	revert_btn.disabled = disabled
	save_btn.disabled = disabled

# Called when the node enters the scene tree for the first time.
func _ready():
	Configuration.applying.connect(_apply)
	await theme_changed
	_apply(Configuration.config)
	dark_mode_opts.selected = Configuration.config.dark_mode

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	#get_viewport().min_size = $Panel/VBoxContainer.get_combined_minimum_size()

func _on_dark_mode_opts_item_selected(index):
	Configuration.config.dark_mode = index
	Configuration.apply_visual()
	disable_revert_save(false)

func _on_save():
	Configuration.save()
	Configuration.apply()
	disable_revert_save(true)

func _on_confirmed():
	_on_save()
	hide()

func _on_theme_changed():
	if dark_mode_opts != null:
		dark_mode_opts.selected = Configuration.config.dark_mode

func _apply(cfg: Config):
	csd_check_btn.set_pressed_no_signal(cfg.enable_csd)
	get_tree().call_group("CSDOpts", "show" if cfg.enable_csd else "hide")

func _on_csd_check_btn_toggled(button_pressed: bool):
	Configuration.config.enable_csd = button_pressed
	Configuration.apply()
	disable_revert_save(false)



func _on_cancel_btn_pressed():
	_on_revert()
	hide()

func _on_revert():
	Configuration.load_config()
	_apply(Configuration.config)
	_on_theme_changed()
	disable_revert_save(true)

