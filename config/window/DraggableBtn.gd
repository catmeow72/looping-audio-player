extends BaseButton

@export
var layout_entry: WinBtnLayout.Entry = WinBtnLayout.Entry.Close

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _get_drag_data(at_position):
	var data: Dictionary = {
		"entry": layout_entry,
		"preview": self.duplicate()
	}
	var preview = self.duplicate(DUPLICATE_SCRIPTS)
	set_drag_preview(preview)
	return data

func _pressed():
	return
