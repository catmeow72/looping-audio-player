class_name CustomMainLoop extends SceneTree

const AUTOLOAD_DIR_PATH = "res://autoload/"
const MAIN_SCENE = "res://main/main_decorated.tscn"

func _initialize():
	async_init()

static func load_async(path: String) -> Resource:
	ResourceLoader.load_threaded_request(path)
	while ResourceLoader.load_threaded_get_status(path) != ResourceLoader.THREAD_LOAD_LOADED:
		await Engine.get_main_loop().process_frame
	return ResourceLoader.load_threaded_get(path)

func async_init():
	pass
